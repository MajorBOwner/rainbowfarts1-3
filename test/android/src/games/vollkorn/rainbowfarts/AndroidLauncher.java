package games.vollkorn.rainbowfarts;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import games.vollkorn.framework.main.AndroidCaller;

public class AndroidLauncher extends AndroidApplication implements AndroidCaller {

    private InterstitialAd mInterstitialAd;

    private GameMain gameMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create the layout
        RelativeLayout layout = new RelativeLayout(this);

        // Do the stuff that initialize() would do for you
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);


        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useWakelock = true;

        //AdMob intersitial
//        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713"); //TODO für release diese ID hier gegen die App spezifische Ad ID austauschen
        MobileAds.initialize(this, "ca-app-pub-4767592729455727~1448737992");
        mInterstitialAd = new InterstitialAd(this);
//        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712"); //TODO für release diese ID hier gegen die App spezifische INTERSITIAL Ad unit ID austauschen
        mInterstitialAd.setAdUnitId("ca-app-pub-4767592729455727/9527595416");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next interstitial.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }

        });

        gameMain = new GameMain(this);
//        initialize(gameMain, config);

        // Create the libgdx View
        View gameView = initializeForView(gameMain, config);

//        // Create and setup the AdMob view
        AdView adView = new AdView(this); // Put in your secret key here
//        adView.loadAd(new AdRequest());

        // Add the libgdx view
        layout.addView(gameView);
        // Add the AdMob view
        RelativeLayout.LayoutParams adParams =
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

        layout.addView(adView, adParams);

        // Hook it all up
        setContentView(layout);


    }

    @Override
    public void showInterstitialAd() {
        gameMain.stopRun();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                }
            }
        });
        gameMain.startRun();
    }
}
