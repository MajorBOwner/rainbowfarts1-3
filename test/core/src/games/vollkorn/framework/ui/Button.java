package games.vollkorn.framework.ui;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

import games.vollkorn.framework.effects.AlphaSpriteEffect;
import games.vollkorn.framework.effects.SpriteEffect;
import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.UpdatableGameObject;

public class Button implements UpdatableGameObject, DrawableGameObject {

    private Sprite unpressed, pressed;
    private boolean isPressed;

    private AlphaSpriteEffect alphaSpriteEffect;

    public Button(TextureRegion unpressedRegion, TextureRegion pressedRegion, float x, float y) {
        unpressed = new Sprite(unpressedRegion);
        pressed = new Sprite(pressedRegion);
        unpressed.setPosition(x - unpressed.getWidth() / 2.f, y - unpressed.getHeight() / 2.f);
        pressed.setPosition(x - pressed.getWidth() / 2.f, y - pressed.getHeight() / 2.f);
        isPressed = false;

        alphaSpriteEffect = new AlphaSpriteEffect(0, 1, 1);
    }

    public void startAlphaEffect(float finalRunTime, float startAlpha, float finalAlpha) {
        alphaSpriteEffect = new AlphaSpriteEffect(finalRunTime, startAlpha, finalAlpha);
        alphaSpriteEffect.start();
    }

    @Override
    public void update(float deltaTime) {
        alphaSpriteEffect.update(deltaTime);
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        prepareSpriteWithEffects(pressed);
        prepareSpriteWithEffects(unpressed);
        if (isPressed) {
            pressed.draw(spriteBatch);
        } else {
            unpressed.draw(spriteBatch);
        }
    }

    private void prepareSpriteWithEffects(Sprite sprite) {
        alphaSpriteEffect.prepareSpriteBeforeDrawing(sprite);
    }

    public boolean isPressed(float x, float y) {
        if (isPressed) {
            if (pressed.getBoundingRectangle().contains(x, y)) {
                isPressed = true;
                return true;
            } else {
                isPressed = false;
                return false;
            }
        } else {
            if (unpressed.getBoundingRectangle().contains(x, y)) {
                isPressed = true;
                return true;
            } else {
                isPressed = false;
                return false;
            }
        }
    }

    public void setPosition(float x, float y){
        unpressed.setPosition(x - unpressed.getWidth() / 2.f, y - unpressed.getHeight() / 2.f);
        pressed.setPosition(x - pressed.getWidth() / 2.f, y - pressed.getHeight() / 2.f);
    }
}
