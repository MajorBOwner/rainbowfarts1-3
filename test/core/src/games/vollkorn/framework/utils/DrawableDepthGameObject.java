package games.vollkorn.framework.utils;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface DrawableDepthGameObject {

    void draw(SpriteBatch spriteBatch);

    float getDepthValue();
}
