package games.vollkorn.framework.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class DrawableSorter {

    private static Comparator comparator = new Comparator<DrawableDepthGameObject>() {
        @Override
        public int compare(DrawableDepthGameObject drawable1, DrawableDepthGameObject drawable2) {
            if (drawable1.getDepthValue() < drawable2.getDepthValue()) {
                return 1;
            } else if (drawable1.getDepthValue() > drawable2.getDepthValue()) {
                return -1;
            } else {
                return 0;
            }
        }
    };

    public static void sortDrawablesByAscendingYCoordinate(ArrayList<DrawableDepthGameObject> drawables) {
        Collections.sort(drawables, comparator);
    }
}
