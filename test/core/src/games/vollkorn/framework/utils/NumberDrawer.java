package games.vollkorn.framework.utils;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;


public class NumberDrawer {
    private Sprite a0, a1, a2, a3, a4, a5, a6, a7, a8, a9;

    public NumberDrawer(TextureRegion t0, TextureRegion t1, TextureRegion t2, TextureRegion t3,
                        TextureRegion t4, TextureRegion t5, TextureRegion t6, TextureRegion t7,
                        TextureRegion t8, TextureRegion t9) {
        a0 = new Sprite(t0);
        a1 = new Sprite(t1);
        a2 = new Sprite(t2);
        a3 = new Sprite(t3);
        a4 = new Sprite(t4);
        a5 = new Sprite(t5);
        a6 = new Sprite(t6);
        a7 = new Sprite(t7);
        a8 = new Sprite(t8);
        a9 = new Sprite(t9);
    }

    public void setColor(float r, float g, float b, float a) {
        a0.setColor(r, g, b, a);
        a1.setColor(r, g, b, a);
        a2.setColor(r, g, b, a);
        a3.setColor(r, g, b, a);
        a4.setColor(r, g, b, a);
        a5.setColor(r, g, b, a);
        a6.setColor(r, g, b, a);
        a7.setColor(r, g, b, a);
        a8.setColor(r, g, b, a);
        a9.setColor(r, g, b, a);
    }

    public void drawFromCenter(SpriteBatch spriteBatch, int number, int digitWidth, float xPos, float yPos) {
        int digitCounter = 0;
        int numberCopy = number;
        if (numberCopy > 0) {
            while (numberCopy > 0) {
                digitCounter++;
                numberCopy /= 10;
            }
        } else {
            digitCounter++;
        }
        float numberHalfWidth = digitWidth * digitCounter / 2;

        digitCounter = 0;
        if (number > 0) {
            while (number > 0) {
                float x = xPos + numberHalfWidth - digitCounter * digitWidth - digitWidth / 2;
                drawDigitAt(spriteBatch, number % 10, x, yPos);
                number /= 10;
                digitCounter++;
            }
        } else {
            drawDigitAt(spriteBatch, 0, xPos, yPos);
        }
    }

    private void drawDigitAt(SpriteBatch spriteBatch, int digit, float x, float y) {
        switch (digit) {
            case 0:
                a0.setPosition(x - a0.getWidth() / 2.0f, y);
                a0.draw(spriteBatch);
                break;
            case 1:
                a1.setPosition(x - a1.getWidth() / 2.0f, y);
                a1.draw(spriteBatch);
                break;
            case 2:
                a2.setPosition(x - a2.getWidth() / 2.0f, y);
                a2.draw(spriteBatch);
                break;
            case 3:
                a3.setPosition(x - a3.getWidth() / 2.0f, y);
                a3.draw(spriteBatch);
                break;
            case 4:
                a4.setPosition(x - a4.getWidth() / 2.0f, y);
                a4.draw(spriteBatch);
                break;
            case 5:
                a5.setPosition(x - a5.getWidth() / 2.0f, y);
                a5.draw(spriteBatch);
                break;
            case 6:
                a6.setPosition(x - a6.getWidth() / 2.0f, y);
                a6.draw(spriteBatch);
                break;
            case 7:
                a7.setPosition(x - a7.getWidth() / 2.0f, y);
                a7.draw(spriteBatch);
                break;
            case 8:
                a8.setPosition(x - a8.getWidth() / 2.0f, y);
                a8.draw(spriteBatch);
                break;
            case 9:
                a9.setPosition(x - a9.getWidth() / 2.0f, y);
                a9.draw(spriteBatch);
                break;
        }
    }
}
