package games.vollkorn.framework.utils;


import com.badlogic.gdx.math.Vector2;

public interface CollidingRectangleObject {

    Vector2 getPosition();

    Rectangle getRectangle();
}
