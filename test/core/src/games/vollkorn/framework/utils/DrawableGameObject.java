package games.vollkorn.framework.utils;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface DrawableGameObject {

    void draw(SpriteBatch spriteBatch);
}
