package games.vollkorn.framework.utils;

import com.badlogic.gdx.math.Vector2;

public class Circle {
    private Vector2 position;
    private float radius;

    public Circle(float x, float y, float radius) {
        position = new Vector2(x, y);
        this.radius = radius;
    }

    public boolean overlapRectangle(Rectangle rectangle) {
        float deltaX = position.x - Math.max(rectangle.getBottomLeftCornerX(), Math.min(position.x, rectangle.getBottomLeftCornerX() + rectangle.getWidth()));
        float deltaY = position.y - Math.max(rectangle.getBottomLeftCornerY(), Math.min(position.y, rectangle.getBottomLeftCornerY() + rectangle.getHeight()));
        return ((deltaX * deltaX + deltaY * deltaY) < (radius * radius));
    }

    public void setCenter(float x, float y) {
        position.x = x;
        position.y = y;
    }

    public void setcenter(Vector2 center) {
        position.x = center.y;
        position.y = center.y;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public void setAttributes(float x, float y, float radius) {
        position.x = x;
        position.y = y;
        this.radius = radius;
    }
}
