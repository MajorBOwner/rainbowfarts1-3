package games.vollkorn.framework.utils;

public interface UpdatableGameObject {

    void update(float deltaTime);
}
