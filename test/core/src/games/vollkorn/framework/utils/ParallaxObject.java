package games.vollkorn.framework.utils;

public interface ParallaxObject {

    void updateParallaxPosition(float relativeX, float relativeY);
}
