package games.vollkorn.framework.utils;


import com.badlogic.gdx.math.Vector2;

public interface CollidingCircleObject {

    Vector2 getPosition();

    Circle getCircle();
}
