package games.vollkorn.framework.utils;

import com.badlogic.gdx.math.Vector2;

public class Rectangle {
    private Vector2 center, bottomLeftCorner;
    private float width, height, halfWidth, halfHeight;

    private Vector2 lastCollisionNormal;
    private Vector2 lastPenetrationDepth;

    public Rectangle(float xPos, float yPos, float width, float height) {
        center = new Vector2(xPos, yPos);
        this.width = width;
        this.height = height;
        halfWidth = width / 2f;
        halfHeight = height / 2f;
        bottomLeftCorner = new Vector2(xPos - halfWidth, yPos - halfHeight);

        lastCollisionNormal = new Vector2(0, 0);
        lastPenetrationDepth = new Vector2(0, 0);
    }

    public boolean overlapRectangle(Rectangle rectangle) {
        return (bottomLeftCorner.x < rectangle.bottomLeftCorner.x + rectangle.width &&
                bottomLeftCorner.x + width > rectangle.bottomLeftCorner.x &&
                bottomLeftCorner.y < rectangle.bottomLeftCorner.y + rectangle.height &&
                bottomLeftCorner.y + height > rectangle.bottomLeftCorner.y);
    }

    public boolean overlapRectangleSaveNormal(Rectangle rectangle) {
        float w = 0.5f * (width + rectangle.width);
        float h = 0.5f * (height + rectangle.height);
        float dx = center.x - rectangle.center.x;
        float dy = center.y - rectangle.center.y;
        if (Math.abs(dx) <= w && Math.abs(dy) <= h) {   //collision
            float wy = w * dy;
            float hx = h * dx;
            if (wy > hx) {
                if (wy > -hx) {   //top collision
                    lastCollisionNormal.x = 0;
                    lastCollisionNormal.y = -1;
                } else {  //left collision
                    lastCollisionNormal.x = 1;
                    lastCollisionNormal.y = 0;
                }
            } else {
                if (wy > -hx) {   //right collision
                    lastCollisionNormal.x = -1;
                    lastCollisionNormal.y = 0;
                } else {  //bottom collision
                    lastCollisionNormal.x = 0;
                    lastCollisionNormal.y = 1;
                }
            }
            lastPenetrationDepth.x = w - Math.abs(dx);
            lastPenetrationDepth.y = h - Math.abs(dy);
            return true;
        }
        return false;
    }

    public Vector2 getLastCollisionNormal() {
        return lastCollisionNormal;
    }

    public Vector2 getLastPenetrationDepth() {
        return lastPenetrationDepth;
    }

    public void setCenter(float xPos, float yPos) {
        center.x = xPos;
        center.y = yPos;
        bottomLeftCorner.x = xPos - halfWidth;
        bottomLeftCorner.y = yPos - halfHeight;
    }

    public void setCenter(Vector2 pos) {
        center.x = pos.x;
        center.y = pos.y;
        bottomLeftCorner.x = pos.x - halfWidth;
        bottomLeftCorner.y = pos.y - halfHeight;
    }

    public void setAttributes(float xPos, float yPos, float width, float height) {
        center.x = xPos;
        center.y = yPos;
        this.width = width;
        this.height = height;
        halfWidth = width / 2f;
        halfHeight = height / 2f;
        bottomLeftCorner.x = xPos - halfWidth;
        bottomLeftCorner.y = yPos - halfHeight;
    }

    public float getBottomLeftCornerX() {
        return bottomLeftCorner.x;
    }

    public float getBottomLeftCornerY() {
        return bottomLeftCorner.y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
}
