package games.vollkorn.framework.effects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;

import java.util.ArrayList;

public class ParticleEffect {

    private ArrayList<Particle> particlePool;
    private ArrayList<Sprite> particleSprites;

    private float emitOffsetMin, emitOffsetMax, emitTimer;

    public ParticleEffect(ArrayList<TextureRegion> regions, float emitOffsetMin, float emitOffsetMax) {
        particleSprites = new ArrayList<Sprite>();
        for (TextureRegion region : regions) {
            particleSprites.add(new Sprite(region));
        }

        particlePool = new ArrayList<Particle>();

        this.emitOffsetMin = emitOffsetMin;
        this.emitOffsetMax = emitOffsetMax;
        if (this.emitOffsetMin < 0.001f) {
            this.emitOffsetMin = 0.001f;
        }
        emitTimer = 0.0f;

        int poolSize = (int) (1.0f / emitOffsetMin);
        if (poolSize > 1000) poolSize = 1000;
        if (poolSize < 50) poolSize = 50;

        for (int i = 0; i < poolSize; ++i) {
            particlePool.add(new Particle());
        }
    }

    public void draw(SpriteBatch spriteBatch) {
        Sprite drawingSprite;
        for (Particle particle : particlePool) {
            if (particle.isActive()) {
                drawingSprite = particleSprites.get(particle.id);
                drawingSprite.setAlpha(particle.alpha);
                drawingSprite.setRotation(particle.rotation);
                drawingSprite.setPosition(particle.xPos, particle.yPos);

                drawingSprite.draw(spriteBatch);
            }
        }
    }

    public void update(float deltaTime) {
        for (Particle particle : particlePool) {
            if (particle.isActive()) {
                particle.update(deltaTime);
            }
        }

        emitTimer += deltaTime;
        if (emitTimer > 60.0f) emitTimer = emitOffsetMin + 0.1f;    //protection against overflow
    }

    public void setColors(ArrayList<Color> spriteColors) {
        if (spriteColors.size() == particleSprites.size()) {
            for (int i = 0; i < particleSprites.size(); ++i) {
                particleSprites.get(i).setColor(spriteColors.get(i));
            }
        }
    }

    public void setColors(float r, float g, float b, float a) {
        for (int i = 0; i < particleSprites.size(); ++i) {
            particleSprites.get(i).setColor(r, g, b, a);
        }
    }

    public boolean isReadyToEmit() {
        return (emitTimer > MathUtils.random(emitOffsetMin, emitOffsetMax));
    }

    public void emit(float xPos, float yPos, float xVel, float yVel, float rotationVelocity, float duration, float alphaStart, float alphaEnd) {
        if (emitTimer > emitOffsetMin) {
            emitTimer = 0.0f;

            int randomId = MathUtils.random(0, (particleSprites.size() - 1));
            Particle particle;
            int counter = 0;
            particle = particlePool.get(counter);
            while (counter < particlePool.size() && particle.isActive()) {
                particle = particlePool.get(counter);
                ++counter;
            }
            if (counter < particlePool.size()) {
                particle.initWithAttributes(randomId, xPos, yPos, xVel, yVel, rotationVelocity, duration, alphaStart, alphaEnd);
                particle.setActive(true);
            }
        }
    }
}
