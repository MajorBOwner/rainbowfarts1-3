package games.vollkorn.framework.effects;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class AlphaSpriteEffect implements SpriteEffect {

    private float runTimer, finalRunTime, startAlpha, finalAlpha;
    private boolean isRunning;

    public AlphaSpriteEffect(float finalRunTime, float startAlpha, float finalAlpha) {
        runTimer = 0;
        this.finalRunTime = finalRunTime;
        this.startAlpha = startAlpha;
        this.finalAlpha = finalAlpha;
        isRunning = false;
    }

    public void start() {
        isRunning = true;
    }

    @Override
    public void update(float deltaTime) {
        if (isRunning) {
            runTimer += deltaTime;
            if (runTimer >= finalRunTime) {
                runTimer = finalRunTime;
                isRunning = false;
            }
        }
    }

    public float getCurrentAlpha() {
        return startAlpha + (1.f / finalRunTime * runTimer * (finalAlpha - startAlpha));
    }

    @Override
    public void prepareSpriteBeforeDrawing(Sprite sprite) {
        if (isRunning) {
            sprite.setAlpha(startAlpha + (1.f / finalRunTime * runTimer * (finalAlpha - startAlpha)));
        } else {
            sprite.setAlpha(finalAlpha);
        }
    }
}
