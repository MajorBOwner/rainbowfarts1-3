package games.vollkorn.framework.effects;

public class Particle {

    public float xPos, yPos, xVel, yVel, rotation, rotationVelocity, duration, alpha, alphaStart, alphaEnd;
    public int id;

    private float particleTimer;

    private boolean isActive;

    public Particle() {
        setActive(false);
    }

    public void update(float deltaTime) {
        xPos += xVel * deltaTime;
        yPos += yVel * deltaTime;

        rotation += rotationVelocity * deltaTime;

        if (alphaStart > alphaEnd) {
            alpha -= (alphaStart - alphaEnd) * deltaTime / duration;
            if (alpha < alphaEnd) alpha = alphaEnd;
        } else {
            alpha += (alphaEnd - alphaStart) * deltaTime / duration;
            if (alpha > alphaEnd) alpha = alphaEnd;
        }

        particleTimer += deltaTime;
        if (particleTimer > duration) setActive(false);
    }

    public void initWithAttributes(int id, float xPos, float yPos, float xVel, float yVel, float rotationVelocity, float duration, float alphaStart, float alphaEnd) {
        this.id = id;
        this.xPos = xPos;
        this.yPos = yPos;
        this.xVel = xVel;
        this.yVel = yVel;
        this.rotationVelocity = rotationVelocity;
        this.duration = duration;
        rotation = 0.0f;
        particleTimer = 0.0f;
        this.alphaStart = alphaStart;
        this.alphaEnd = alphaEnd;
        alpha = this.alphaStart;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean isActive() {
        return isActive;
    }
}
