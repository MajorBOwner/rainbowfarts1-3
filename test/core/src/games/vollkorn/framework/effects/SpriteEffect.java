package games.vollkorn.framework.effects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface SpriteEffect {

    void update(float deltaTime);

    void prepareSpriteBeforeDrawing(Sprite sprite);

//    public enum EffectType {
//        BOUNCY_SQUISH(0);
//
//        public int value;
//
//        EffectType(int value){
//            this.value = value;
//        }
//    }
//
//    private int effectTypeCode;
//
//    private float[][] valuesLUT;    //first array identifies the effect, second array holds the values for the related effect
//
//    private float animationTimer, speed;
//
//    private int valueStepCount;
//
//    private boolean isRunning;
//
//    public SpriteEffect(int valueStepCount) {
//        this.valueStepCount = valueStepCount;
//        animationTimer = 0.0f;
//        valuesLUT = new float[1][this.valueStepCount];  //fixed value resembles the count of all effects
//        effectTypeCode = 0;
//    }
//
//    public void setEffectParameters(EffectType effectType, float speed) {
//        effectTypeCode = 0;
//        this.speed = speed;
//
//        switch (effectType){
//            case BOUNCY_SQUISH: {
//                effectTypeCode += 10;
//                break;
//            }
//        }
//
//        initLUT();
//    }
//
//    public void setEffectParameters(EffectType[] effectTypes, float speed){
//        effectTypeCode = 0;
//        this.speed = speed;
//
//        boolean bouncysquishSet = false;
//
//        for(EffectType effectType : effectTypes){
//            switch (effectType){
//                case BOUNCY_SQUISH: {
//                    if(!bouncysquishSet){
//                        effectTypeCode += 10;
//                        bouncysquishSet = true;
//                    }
//                    break;
//                }
//            }
//        }
//
//        initLUT();
//    }
//
//    public void resetEffect() {
//        animationTimer = 0.0f;
//    }
//
//    public void update(float deltaTime) {
//        animationTimer += deltaTime;
//        if (animationTimer >= speed) {
//            isRunning = false;
//            resetEffect();
//        }
//    }
//
//    public void start() {
//        isRunning = true;
//        resetEffect();
//    }
//
//    public boolean isRunning() {
//        return isRunning;
//    }
//
//    public void draw(SpriteBatch spriteBatch, Sprite sprite) {
//        float ori_x1 = sprite.getVertices()[Batch.X1];
//        float ori_y1 = sprite.getVertices()[Batch.Y1];
//        float ori_x2 = sprite.getVertices()[Batch.X2];
//        float ori_y2 = sprite.getVertices()[Batch.Y2];
//        float ori_x3 = sprite.getVertices()[Batch.X3];
//        float ori_y3 = sprite.getVertices()[Batch.Y3];
//        float ori_x4 = sprite.getVertices()[Batch.X4];
//        float ori_y4 = sprite.getVertices()[Batch.Y4];
//
//        float ori_posX = sprite.getX();
//        float ori_posY = sprite.getY();
//
//        handleEffects(sprite);
//
//        sprite.draw(spriteBatch);
//
//        sprite.setPosition(ori_posX, ori_posY);
//
//        sprite.getVertices()[Batch.X1] = ori_x1;
//        sprite.getVertices()[Batch.Y1] = ori_y1;
//        sprite.getVertices()[Batch.X2] = ori_x2;
//        sprite.getVertices()[Batch.Y2] = ori_y2;
//        sprite.getVertices()[Batch.X3] = ori_x3;
//        sprite.getVertices()[Batch.Y3] = ori_y3;
//        sprite.getVertices()[Batch.X4] = ori_x4;
//        sprite.getVertices()[Batch.Y4] = ori_y4;
//    }
//
//    private void handleEffects(Sprite sprite){
//        switch (effectTypeCode){
//            //BOUNCY_SQUISH
//            case 10: {
//                applyBouncy_Squish(sprite);
//                break;
//            }
//        }
//    }
//
//    private void applyBouncy_Squish(Sprite sprite) {
//        float timeStep = (animationTimer / speed) * valueStepCount;
//        float squishAmount = valuesLUT[EffectType.BOUNCY_SQUISH.value][(int) (timeStep)] - 1.0f;
//        squishAmount = squishAmount * (sprite.getVertices()[Batch.Y2] - sprite.getVertices()[Batch.Y1]);
//
//        sprite.getVertices()[Batch.Y2] = sprite.getVertices()[Batch.Y2] + squishAmount;
//        sprite.getVertices()[Batch.Y3] = sprite.getVertices()[Batch.Y3] + squishAmount;
//
//        squishAmount = valuesLUT[EffectType.BOUNCY_SQUISH.value][(int) (timeStep)] - 1.0f;
//        squishAmount = squishAmount * (sprite.getVertices()[Batch.X4] - sprite.getVertices()[Batch.X1]) * 0.5f;
//        sprite.getVertices()[Batch.X3] = sprite.getVertices()[Batch.X3] - squishAmount;
//        sprite.getVertices()[Batch.X4] = sprite.getVertices()[Batch.X4] - squishAmount;
//
//        sprite.setPosition(sprite.getX() + squishAmount / 2.0f, sprite.getY() - squishAmount / 2.0f);
//    }
//
//    private void initLUT() {
//        switch (effectTypeCode) {
//            //BOUNCY_SQUISH
//            case 10: {
//                for (int i = 0; i < valueStepCount; ++i) {
//                    valuesLUT[EffectType.BOUNCY_SQUISH.value][i] = getBouncy_SquishValue(((float)i / (float)valueStepCount));
//                }
//                break;
//            }
//        }
//    }
//
//    private float getBouncy_SquishValue(float x) {
//        return (float) (3956.74 * Math.pow(x, 9.0f) - 21536.73 * Math.pow(x, 8.0f) + 48581.73 * Math.pow(x, 7.0f) - 58822.43 * Math.pow(x, 6.0f)
//                + 41263.69 * Math.pow(x, 5.0f) - 16808.46 * Math.pow(x, 4.0f) + 3735.92 * Math.pow(x, 3.0f) - 380.38 * Math.pow(x, 2.0f) + 9.93 * x + 0.99);
//    }
}
