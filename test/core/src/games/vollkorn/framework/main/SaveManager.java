package games.vollkorn.framework.main;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.JsonWriter;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class SaveManager {

    private JsonObject saveObject;

    public SaveManager() {
        saveObject = new JsonObject();
    }

    public void load(){
        try {
            Reader r = Gdx.files.local("save.json").reader();
            JsonParser jsonParser = new JsonParser();
            saveObject = jsonParser.parse(r).getAsJsonObject();
            try {
                r.close();
            }catch (IOException e){
                //fucking close did not work
            }
        }catch(GdxRuntimeException e){
            //could not load
            saveObject = new JsonObject();
        }
    }

    public void save(){
        try {
            Writer w = Gdx.files.local("save.json").writer(false);
            JsonWriter jsonWriter = new JsonWriter(w);
            Gson gson = new Gson();
            gson.toJson(saveObject, jsonWriter);
            try{
                w.close();
            }catch (IOException e){
                //fucking close did not work
            }
            try {
                jsonWriter.close();
            }catch (IOException e){
                //fucking clode did not work
            }
        }catch (GdxRuntimeException e){
            //could not save
        }
    }

    public int getInt(String key){
        int value = 0;
        if(saveObject.has(key)){
            value = saveObject.get(key).getAsInt();
        }
        return value;
    }

    public float getFloat(String key){
        float value = 0f;
        if(saveObject.has(key)){
            value = saveObject.get(key).getAsFloat();
        }
        return value;
    }

    public boolean getBoolean(String key){
        boolean value = false;
        if(saveObject.has(key)){
            value = saveObject.get(key).getAsBoolean();
        }
        return value;
    }

    public String getString(String key){
        String value = "";
        if (saveObject.has(key)){
            value = saveObject.get(key).getAsString();
        }
        return value;
    }

    public void putInt(String key, int value){
        saveObject.addProperty(key, value);
    }

    public void putFloat(String key, float value){
        saveObject.addProperty(key, value);
    }

    public void putBoolean(String key, boolean value){
        saveObject.addProperty(key, value);
    }

    public void putString(String key, String value){
        saveObject.addProperty(key, value);
    }

    public boolean hasEntry(String key){
        return saveObject.has(key);
    }

//    public int getCoins() {
//        return saveObject.coins;
//    }
//
//    public void addCoins(int c) {
//        saveObject.coins += c;
//    }
//
//    public void subtractCoins(int c) {
//        saveObject.coins -= c;
//    }
//
//    public boolean isFirstStart(){
//        return saveObject.firstStart;
//    }
//
//    public void hadFirstStart(){
//        saveObject.firstStart = false;
//    }
//
//
//    public void boughtSmallFireFart(){
//        saveObject.boughtSmallFireFart = true;
//    }
//
//    public void boughtSmallAtomFart(){
//        saveObject.boughtSmallAtomFart = true;
//    }
//
//    public void boughtMidNormalFart(){
//        saveObject.boughtMidNormalFart = true;
//    }
//
//    public void boughtMidFireFart(){
//        saveObject.boughtMidFireFart = true;
//    }
//
//    public void boughtMidAtomFart(){
//        saveObject.boughtMidAtomFart = true;
//    }
//
//    public void boughtBigNormalFart(){
//        saveObject.boughtBigNormalFart = true;
//    }
//
//    public void boughtBigFireFart(){
//        saveObject.boughtBigFireFart = true;
//    }
//
//    public void boughtBigAtomfart(){
//        saveObject.boughtBigAtomfart = true;
//    }
//
//    public boolean isSmallFireFartBought(){
//        return saveObject.boughtSmallFireFart;
//    }
//
//    public boolean isSmallAtomFartBought(){
//        return saveObject.boughtSmallAtomFart;
//    }
//
//    public boolean isMidNormalFartBought(){
//        return saveObject.boughtMidNormalFart;
//    }
//
//    public boolean isMidFireFartBought(){
//        return saveObject.boughtMidFireFart;
//    }
//
//    public boolean isMidAtomFartBought(){
//        return saveObject.boughtMidAtomFart;
//    }
//
//    public boolean isBigNormalFartBought(){
//        return saveObject.boughtBigNormalFart;
//    }
//
//    public boolean isBigFireFartBought(){
//        return saveObject.boughtBigFireFart;
//    }
//
//    public boolean isBigAtomFartBought(){
//        return saveObject.boughtBigAtomfart;
//    }
}
