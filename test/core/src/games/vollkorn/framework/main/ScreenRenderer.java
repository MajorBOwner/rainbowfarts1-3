package games.vollkorn.framework.main;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class ScreenRenderer implements Disposable {

    /*
    Short summary of all possible viewports:
    ExtendViewport - keeps the aspect ratio (aka objects will scale down/up if screensize scales), but if the screen resolution differs, the viewport extends
    FillViewport - keeps the aspect ratio by scaling the world up to take the whole screen (some of the world may be off screen)
    FitViewport - keeps the aspect ratio by scaling the world up to fit the screen, adding black bars (letterboxing) for the remaining space
    StretchViewport - does not keep the aspect ratio, the world is scaled to take the whole screen
    ScreenViewport - does not keep aspect ratio (aka objects will not get scaled down/up if screensize scales) / the world size is based on the screen size;
                    also, viewport will just extend or compress if the screen resolution changes in one direction
     */
//    private Viewport viewport;
//
//    private OrthographicCamera camera;

    private SpriteBatch spriteBatch;
    private AbstractScreenController screenController;

    //for debug rendering purposes
//    public Matrix4 projectionMatrix;

    public ScreenRenderer(AbstractScreenController screenController) {
        this.screenController = screenController;

        spriteBatch = new SpriteBatch();
//        camera = new OrthographicCamera(ConfigConsts.VIEWPORTWIDTH, ConfigConsts.VIEWPORTHEIGHT);
//
////        viewport = new ExtendViewport(camera.viewportWidth, camera.viewportHeight, camera);
//        viewport = new FitViewport(camera.viewportWidth, camera.viewportHeight, camera);
//        viewport.apply();
//        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    public void render(int renderStage) {
//        screenController.cameraHelper.applyTo(camera);

//        spriteBatch.setProjectionMatrix(camera.combined);
        //for debug rendering purposes
//        projectionMatrix = spriteBatch.getProjectionMatrix().cpy().scl(ConfigConsts.PIX2M);

        spriteBatch.setProjectionMatrix(screenController.getCameraAccordingToRenderStage(renderStage).combined);


        spriteBatch.begin();
        screenController.render(spriteBatch, renderStage);
        spriteBatch.end();
    }

//    public OrthographicCamera getCamera(){
//        return camera;
//    }

//    public void resize(int width, int height){
//        viewport.update(width, height);
//    }

    @Override
    public void dispose() {
        spriteBatch.dispose();
    }
}
