package games.vollkorn.framework.main;


import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;

public class CameraHelper {
    private Vector3 position;
    private float zoom;
    private float rotation;

    private Vector3 snapshotposition;
    private float snapshotzoom;
    private float snapshotrotation;

    public CameraHelper() {
        position = new Vector3();
        zoom = 1;
        rotation = 0;

        snapshotposition = new Vector3();
        snapshotzoom = 1;
        snapshotrotation = 0;
    }

    public void update(float deltaTime) {
        //automatisiertes Kameraverhalten
    }

    public void setPosition(float x, float y, float z) {
        this.position.set(x, y, z);
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }

    public void setRadRotation(float rotation) {
        this.rotation = rotation;
    }

    public Vector3 getPosition() {
        return position;
    }

    public float getZoom() {
        return zoom;
    }

    public float getRadRotation(){
        return rotation;
    }

    public float getDegRotation(){
        return MathUtils.radiansToDegrees * rotation;
    }

    public void resetCameraToZeroValues(){
        position.x = 0;
        position.y = 0;
        position.z = 0;
        zoom = 1;
        rotation = 0;
    }

    public void snapshotCameraValues(){
        snapshotposition.x = position.x;
        snapshotposition.y = position.y;
        snapshotposition.z = position.z;
        snapshotzoom = zoom;
        snapshotrotation = rotation;
    }

    public void revertCameraValuesToSnapshot(){
        position.x = snapshotposition.x;
        position.y = snapshotposition.y;
        position.z = snapshotposition.z;
        zoom = snapshotzoom;
        rotation = snapshotrotation;
    }

    public void lerp(float targetx, float targety, float targetz, float alpha) {
        position.x += alpha * (targetx - position.x);
        position.y += alpha * (targety - position.y);
        position.z += alpha * (targetz - position.z);
    }

//    public void setTarget (Sprite target) { this.target = target; }
//    public Sprite getTarget () { return target; }
//    public boolean hasTarget () { return target != null; }
//    public boolean hasTarget (Sprite target) {
//        return hasTarget() && this.target.equals(target);
//    }

    public void applyTo(OrthographicCamera camera) {
        camera.position.x = position.x;
        camera.position.y = position.y;
        camera.zoom = this.zoom;
        camera.up.set(0, 1, 0);
        camera.direction.set(0, 0, -1);
        camera.rotate(MathUtils.radiansToDegrees * rotation);
        camera.update();
    }
}
