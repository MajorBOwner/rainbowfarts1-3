package games.vollkorn.framework.main;


public class ConfigConsts {

    public static final float VIEWPORTWIDTH = 1080;
    public static final float VIEWPORTHEIGHT = 1920;

    public static final String TEXTURE_ATLAS_ANDROID = "visuals/fart.atlas";
    public static final String MUSIC_PATH_ANDROID = "sound/";

    public static final String TEXTURE_ATLAS_DESKTOP = "assets/visuals/fart.atlas";
    public static final String MUSIC_PATH_DESKTOP = "assets/sound/";

    public static final int RENDER_STAGES = 2;

    public static final float BASE_RED = 0;
    public static final float BASE_GREEN = 0;
    public static final float BASE_BLUE = 0;
    public static final float BASE_ALPHA = 1;

    public static final float TIMESTEP = 0.01f;
}
