package games.vollkorn.framework.main;


import com.badlogic.gdx.Screen;


public abstract class AbstractGameScreen implements Screen {
    protected GameHeart game;

    public AbstractGameScreen (GameHeart game) {
        this.game = game;
    }

    @Override
    public abstract void render (float deltaTime);
    @Override
    public abstract void resize (int width, int height);
    @Override
    public abstract void show ();
    @Override
    public abstract void hide ();
    @Override
    public abstract void pause ();
    @Override
    public void resume () {
    }
    @Override
    public void dispose () {
    }
}
