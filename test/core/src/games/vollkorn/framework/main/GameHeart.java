package games.vollkorn.framework.main;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import games.vollkorn.framework.assets.SoundAssets;
import games.vollkorn.framework.assets.VisualAssets;

public class GameHeart extends Game {
    private AssetManager assetManager;
    private VisualAssets visualAssets;
    private SoundAssets soundAssets;

    public SaveManager saveManager;
    public AndroidCaller androidCaller;

    private boolean isRunning;

    public GameHeart() {
        startRun();
    }

    public GameHeart(AndroidCaller androidCaller) {
        startRun();
        this.androidCaller = androidCaller;
    }

    public void stopRun() {
        isRunning = false;
    }

    public void startRun() {
        isRunning = true;
    }

    @Override
    public void create() {
        initAssets();
        saveManager = new SaveManager();
    }

    @Override
    public void render() {
        if (isRunning) {
            super.render();
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        assetManager.dispose();
        visualAssets.dispose();
    }

    private void initAssets() {
        assetManager = new AssetManager();
        //Loading Texture Atlas
        String assetPath = "";
        String musicAssetPath = "";
        switch (Gdx.app.getType()) {
            case Android:
                assetPath = ConfigConsts.TEXTURE_ATLAS_ANDROID;
                musicAssetPath = ConfigConsts.MUSIC_PATH_ANDROID;
                break;
            case Desktop:
                assetPath = ConfigConsts.TEXTURE_ATLAS_DESKTOP;
                musicAssetPath = ConfigConsts.MUSIC_PATH_DESKTOP;
                break;
            case HeadlessDesktop:
                break;
            case Applet:
                break;
            case WebGL:
                break;
            case iOS:
                break;
        }
        assetManager.load(assetPath, TextureAtlas.class);
        assetManager.load(musicAssetPath + "rfgamemusic.ogg", Sound.class);
        assetManager.load(musicAssetPath + "fart.ogg", Sound.class);
        assetManager.load(musicAssetPath + "fartstutter.ogg", Sound.class);
        assetManager.load(musicAssetPath + "fartstop.ogg", Sound.class);
        assetManager.load(musicAssetPath + "burp.ogg", Sound.class);
        assetManager.load(musicAssetPath + "explosion.ogg", Sound.class);

        //Loading Assets until finished
        assetManager.finishLoading();

        TextureAtlas atlas = assetManager.get(assetPath);
        // enable texture filtering for pixel smoothing
        for (Texture t : atlas.getTextures()) {
            t.setFilter(Texture.TextureFilter.MipMapLinearNearest, Texture.TextureFilter.Nearest);
            t.setWrap(Texture.TextureWrap.ClampToEdge, Texture.TextureWrap.ClampToEdge);
        }

        //Creating Game Resource Objects
        visualAssets = new VisualAssets(atlas);
        soundAssets = new SoundAssets(assetManager, musicAssetPath);
    }
}
