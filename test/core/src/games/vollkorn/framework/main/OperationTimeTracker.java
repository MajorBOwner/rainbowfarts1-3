package games.vollkorn.framework.main;

public class OperationTimeTracker {

    private long startTimeMillis, startTimeNano;

    public void startMillis(){
        startTimeMillis = System.currentTimeMillis();
    }

    public long endMillis(){
        return (System.currentTimeMillis() - startTimeMillis);
    }

    public void startNano(){
        startTimeNano = System.nanoTime();
    }

    public long endNano(){
        return (System.nanoTime() - startTimeNano);
    }
}
