package games.vollkorn.framework.main;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class AbstractScreenController implements InputProcessor {

    protected GameHeart game;
//    public CameraHelper cameraHelper;
    protected Screen screen;

    public AbstractScreenController(GameHeart game, Screen screen) {
        this.game = game;
        this.screen = screen;

        Gdx.input.setInputProcessor(this);
//        cameraHelper = new CameraHelper();
//
//        cameraHelper.setPosition(0, 0, 0);
//        cameraHelper.setZoom(1.0f);
    }

    public abstract void update(float deltaTime);

    public abstract void render(SpriteBatch spriteBatch, int renderStage);

    public abstract OrthographicCamera getCameraAccordingToRenderStage(int renderStage);

    public abstract void resizeAllViewports(int width, int height);

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
