package games.vollkorn.framework.assets;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;

public class SoundAssets {
    public static Sound gamemusic;
    public static Sound fart;
    public static Sound fartstop;
    public static Sound fartstutter;
    public static Sound burp;
    public static Sound explosion;

    public static boolean isSoundOn;

    public SoundAssets(AssetManager assetManager, String musicAssetPath) {
        gamemusic = assetManager.get(musicAssetPath + "rfgamemusic.ogg", Sound.class);
        fart = assetManager.get(musicAssetPath + "fart.ogg", Sound.class);
        fartstop = assetManager.get(musicAssetPath + "fartstop.ogg", Sound.class);
        fartstutter = assetManager.get(musicAssetPath + "fartstutter.ogg", Sound.class);
        burp = assetManager.get(musicAssetPath + "burp.ogg", Sound.class);
        explosion = assetManager.get(musicAssetPath + "explosion.ogg", Sound.class);
    }
}
