package games.vollkorn.framework.assets;


import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import games.vollkorn.framework.utils.NumberDrawer;

public class VisualAssets implements Disposable {
    public static TextureRegion vollkorngameslogo, vollkorngamesname, whiteback;

    public static TextureRegion zero, one, two, three, four, five, six, seven, eight, nine;
    public static TextureRegion down1, down2, down3, down4, fart4, fart5, fart6, peg1, peg2, peg3, peg4, peg5, peg6, peg7, peg8;
    public static TextureRegion explosion1, explosion2, explosion3, explosion4, explosion5, explosion6, explosion7, explosion8, explosion9;
    public static Animation<TextureRegion> explosionAni;
    public static TextureRegion fartcloud1, fartcloud2, fartcloud3, fartcloud4, fartcloud5, fartcloud6;
    public static TextureRegion cloud1, cloud2, cloud3, cloud4, cloud5, cloud6;
    public static TextureRegion background;
    public static TextureRegion food1, food2, food3, food4, food5, rfstar;
    public static Animation<TextureRegion> fartAni, fallAni, pegasusAni;
    public static TextureRegion play, shopicorn, noad, rank, rate, soundoff, soundon, logo1, logo2, mountain1, mountain2, grass1, grass2, shrub1, shrub2, tree1, tree2, tree3, star;
    public static Animation<TextureRegion> logoAnimation;
    public static TextureRegion fartbar1, fartbar2, fartbar3;

//    public static BitmapFont pointsFont;

    public static NumberDrawer pointsDrawer;

    public VisualAssets(TextureAtlas atlas) {
        vollkorngameslogo = atlas.findRegion("vollkorngameslogo");
        vollkorngamesname = atlas.findRegion("vollkorngamesname");
        whiteback = atlas.findRegion("whiteback");

        zero = atlas.findRegion("0");
        one = atlas.findRegion("1");
        two = atlas.findRegion("2");
        three = atlas.findRegion("3");
        four = atlas.findRegion("4");
        five = atlas.findRegion("5");
        six = atlas.findRegion("6");
        seven = atlas.findRegion("7");
        eight = atlas.findRegion("8");
        nine = atlas.findRegion("9");
        pointsDrawer = new NumberDrawer(zero, one, two, three, four, five, six, seven, eight, nine);

        down1 = atlas.findRegion("down1");
        down2 = atlas.findRegion("down2");
        down3 = atlas.findRegion("down3");
        down4 = atlas.findRegion("down4");

        fart4 = atlas.findRegion("fart4");
        fart5 = atlas.findRegion("fart5");
        fart6 = atlas.findRegion("fart6");

        explosion1 = atlas.findRegion("explosion1");
        explosion2 = atlas.findRegion("explosion2");
        explosion3 = atlas.findRegion("explosion3");
        explosion4 = atlas.findRegion("explosion4");
        explosion5 = atlas.findRegion("explosion5");
        explosion6 = atlas.findRegion("explosion6");
        explosion7 = atlas.findRegion("explosion7");
        explosion8 = atlas.findRegion("explosion8");
        explosion9 = atlas.findRegion("explosion9");

        peg1 = atlas.findRegion("adult1");
        peg2 = atlas.findRegion("adult2");
        peg3 = atlas.findRegion("adult3");
        peg4 = atlas.findRegion("adult4");
        peg5 = atlas.findRegion("adult5");
        peg6 = atlas.findRegion("adult6");
        peg7 = atlas.findRegion("adult7");
        peg8 = atlas.findRegion("adult8");

        cloud1 = atlas.findRegion("cloud1");
        cloud2 = atlas.findRegion("cloud2");
        cloud3 = atlas.findRegion("cloud3");
        cloud4 = atlas.findRegion("cloud4");
        cloud5 = atlas.findRegion("cloud5");
        cloud6 = atlas.findRegion("cloud6");

        mountain1 = atlas.findRegion("mountain1");
        mountain2 = atlas.findRegion("mountain2");

        tree1 = atlas.findRegion("tree1");
        tree2 = atlas.findRegion("tree2");
        tree3 = atlas.findRegion("tree3");

        shrub1 = atlas.findRegion("shrub1");
        shrub2 = atlas.findRegion("shrub2");

        grass1 = atlas.findRegion("grass1");
        grass2 = atlas.findRegion("grass2");

        star = atlas.findRegion("star");

        background = atlas.findRegion("background");

        fartcloud1 = atlas.findRegion("farticle1");
        fartcloud2 = atlas.findRegion("farticle2");
        fartcloud3 = atlas.findRegion("farticle3");
        fartcloud4 = atlas.findRegion("farticle4");
        fartcloud5 = atlas.findRegion("farticle5");
        fartcloud6 = atlas.findRegion("farticle6");

        food1 = atlas.findRegion("cabbage");
        food2 = atlas.findRegion("beans");
        food3 = atlas.findRegion("onion");
        food4 = atlas.findRegion("milk");
        food5 = atlas.findRegion("pizza");
        rfstar = atlas.findRegion("stern");

        play = atlas.findRegion("play");
        shopicorn = atlas.findRegion("Einhorn");
        noad = atlas.findRegion("noad");
        rank = atlas.findRegion("rank");
        rate = atlas.findRegion("rate");
        soundon = atlas.findRegion("soundon");
        soundoff = atlas.findRegion("soundoff");
        logo1 = atlas.findRegion("logo1");
        logo2 = atlas.findRegion("logo2");

        Array<TextureRegion> frames = new Array<TextureRegion>();
        frames.add(down1);
        frames.add(down2);
        frames.add(down3);
        frames.add(down4);
        fallAni = new Animation<TextureRegion>(0.1f, frames);
        fallAni.setPlayMode(Animation.PlayMode.LOOP);

        frames.clear();
        frames.add(fart4);
        frames.add(fart5);
        frames.add(fart6);
        fartAni = new Animation<TextureRegion>(0.1f, frames);
        fartAni.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);

        frames.clear();
        frames.add(peg1);
        frames.add(peg2);
        frames.add(peg3);
        frames.add(peg4);
        frames.add(peg5);
        frames.add(peg6);
        frames.add(peg7);
        frames.add(peg8);
        pegasusAni = new Animation<TextureRegion>(0.1f, frames);
        pegasusAni.setPlayMode(Animation.PlayMode.LOOP);

        frames.clear();
        frames.add(logo1);
        frames.add(logo2);
        logoAnimation = new Animation<TextureRegion>(0.2f, frames);
        logoAnimation.setPlayMode(Animation.PlayMode.LOOP);

        frames.clear();
        frames.add(explosion1);
        frames.add(explosion2);
        frames.add(explosion3);
        frames.add(explosion4);
        frames.add(explosion5);
        frames.add(explosion6);
        frames.add(explosion7);
        frames.add(explosion8);
        frames.add(explosion9);
        explosionAni = new Animation<TextureRegion>(0.05f, frames);
        explosionAni.setPlayMode(Animation.PlayMode.LOOP);

        fartbar1 = atlas.findRegion("fartbar1");
        fartbar2 = atlas.findRegion("fartbar2");
        fartbar3 = atlas.findRegion("fartbar3");

//        FreeTypeFontGenerator freeTypeFontGenerator = new FreeTypeFontGenerator(Gdx.files.local("assets/fonts/Scada-Regular.ttf"));
//        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
//        parameter.color = new Color(255f / 255f, 184f / 255f, 121f / 255f, 1);
//        parameter.borderColor = Color.BLACK;
//        parameter.borderWidth = 5;
//        parameter.size = 80;
//        parameter.genMipMaps = true;
//        parameter.minFilter = Texture.TextureFilter.MipMapNearestNearest;
//        parameter.magFilter = Texture.TextureFilter.MipMapNearestNearest;
//        pointsFont = freeTypeFontGenerator.generateFont(parameter);
//        freeTypeFontGenerator.dispose();
    }

    @Override
    public void dispose() {
//        pointsFont.dispose();
    }

//    public static void drawPointsFont(SpriteBatch spriteBatch, String text, float x, float y, float r, float g, float b, float a){
//        VisualAssets.pointsFont.setColor(r, g, b, a);
//        VisualAssets.pointsFont.draw(spriteBatch, text, x, y);
//    }
}
