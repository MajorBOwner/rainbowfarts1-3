package games.vollkorn.rainbowfarts.landingpage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import games.vollkorn.framework.assets.VisualAssets;
import games.vollkorn.framework.main.AbstractScreenController;
import games.vollkorn.framework.main.ConfigConsts;
import games.vollkorn.framework.main.GameHeart;
import games.vollkorn.rainbowfarts.game.GameScreen;

public class LandingController extends AbstractScreenController {

    private enum LogoState {
        FADIN,
        SHOW,
        FADEOUT
    }

    private LogoState logoState;

    private OrthographicCamera camera;
    private Viewport viewport;

    private Sprite vollkorngameslogo, vollkorngamesname, whiteback;
    private float logoAlpha, signAlpha, showTimer;

    public LandingController(GameHeart game, Screen screen) {
        super(game, screen);
        camera = new OrthographicCamera(ConfigConsts.VIEWPORTWIDTH, ConfigConsts.VIEWPORTHEIGHT);
        viewport = new FitViewport(camera.viewportWidth, camera.viewportHeight, camera);
        viewport.apply();
        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.x = 0;
        camera.position.y = 0;
        camera.position.z = 0;
        camera.zoom = 1;

        logoAlpha = 0;
        signAlpha = 0;
        showTimer = 0;
        vollkorngameslogo = new Sprite(VisualAssets.vollkorngameslogo);
        vollkorngameslogo.setPosition(-vollkorngameslogo.getWidth() / 2, -vollkorngameslogo.getHeight() / 2 + 300);
        vollkorngamesname = new Sprite(VisualAssets.vollkorngamesname);
        vollkorngamesname.setPosition(-vollkorngamesname.getWidth() / 2, -vollkorngamesname.getHeight() / 2 - 400);
        whiteback = new Sprite(VisualAssets.whiteback);
        whiteback.setScale(3000, 3000);
        whiteback.setPosition(-whiteback.getWidth() / 2, -whiteback.getHeight() / 2);

        logoState = LogoState.FADIN;
    }

//    public void setCamera(OrthographicCamera camera) {
//        this.camera = camera;
//    }


    @Override
    public void update(float deltaTime) {
        showTimer += deltaTime;
        if(showTimer >= 1) {
            switch (logoState) {
                case FADIN:
                    logoAlpha += deltaTime;
                    if (logoAlpha >= 1) {
                        logoAlpha = 1;
                        logoState = LogoState.SHOW;
                    }
                    break;
                case SHOW:
                    signAlpha += deltaTime;
                    if (signAlpha >= 1) {
                        signAlpha = 1;
                        logoState = LogoState.FADEOUT;
                    }
                    break;
                case FADEOUT:
                    logoAlpha -= deltaTime * 2;
                    signAlpha -= deltaTime * 2;
                    if (logoAlpha <= 0) {
                        logoAlpha = 0;
                    }
                    if (signAlpha <= 0) {
                        signAlpha = 0;
                    }
                    break;
            }
        }
    }

    @Override
    public void render(SpriteBatch spriteBatch, int renderStage) {
        whiteback.draw(spriteBatch);
        vollkorngameslogo.setAlpha(logoAlpha);
        vollkorngameslogo.draw(spriteBatch);
        vollkorngamesname.setAlpha(signAlpha);
        vollkorngamesname.draw(spriteBatch);
        if (logoState == LogoState.FADEOUT && logoAlpha <= 0 && signAlpha <= 0) {
            game.setScreen(new GameScreen(game));
        }
    }

    @Override
    public OrthographicCamera getCameraAccordingToRenderStage(int renderStage) {
        return camera;
    }

    @Override
    public void resizeAllViewports(int width, int height) {
        viewport.update(width, height);
    }
}
