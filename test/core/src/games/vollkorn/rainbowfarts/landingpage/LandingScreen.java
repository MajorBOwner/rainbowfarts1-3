package games.vollkorn.rainbowfarts.landingpage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

import games.vollkorn.framework.main.AbstractGameScreen;
import games.vollkorn.framework.main.ConfigConsts;
import games.vollkorn.framework.main.GameHeart;
import games.vollkorn.framework.main.ScreenRenderer;

public class LandingScreen extends AbstractGameScreen {

    private LandingController landingController;
    private ScreenRenderer screenRenderer;

    private float timeAccumulator;

    public LandingScreen(GameHeart game) {
        super(game);
        timeAccumulator = 0.0f;
    }

    @Override
    public void render(float deltaTime) {
        timeAccumulator += deltaTime;
        while (timeAccumulator >= ConfigConsts.TIMESTEP) {
            landingController.update(ConfigConsts.TIMESTEP);

            timeAccumulator -= ConfigConsts.TIMESTEP;
        }

        Gdx.gl.glClearColor(ConfigConsts.BASE_RED, ConfigConsts.BASE_GREEN, ConfigConsts.BASE_BLUE, ConfigConsts.BASE_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        for(int i = 0; i < ConfigConsts.RENDER_STAGES; i++){
            // Render game world to screen
            screenRenderer.render(i);
        }
    }

    @Override
    public void resize(int width, int height) {
        landingController.resizeAllViewports(width, height);
    }

    @Override
    public void show() {
        landingController = new LandingController(game, this);
        screenRenderer = new ScreenRenderer(landingController);
//        landingController.setCamera(screenRenderer.getCamera());
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void hide() {
        screenRenderer.dispose();
        Gdx.input.setCatchBackKey(false);
    }

    @Override
    public void pause() {
        Gdx.app.exit();
    }
}
