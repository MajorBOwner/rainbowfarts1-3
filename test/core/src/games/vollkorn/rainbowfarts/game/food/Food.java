package games.vollkorn.rainbowfarts.game.food;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import games.vollkorn.framework.assets.VisualAssets;
import games.vollkorn.framework.utils.CollidingRectangleObject;
import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.Rectangle;

public class Food implements DrawableGameObject, CollidingRectangleObject {

    private Sprite sprite;
    private Rectangle rectangle;
    private boolean gotEaten;

    private boolean isRainbofartItem;

    public Food(float x, float y, boolean isRainbofartItem) {
        this.isRainbofartItem = isRainbofartItem;
        if (this.isRainbofartItem) {
            sprite = new Sprite(VisualAssets.rfstar);
        } else {
            int r = MathUtils.random(1, 5);
            switch (r) {
                case 1:
                    sprite = new Sprite(VisualAssets.food1);
                    break;
                case 2:
                    sprite = new Sprite(VisualAssets.food2);
                    break;
                case 3:
                    sprite = new Sprite(VisualAssets.food3);
                    break;
                case 4:
                    sprite = new Sprite(VisualAssets.food4);
                    break;
                case 5:
                    sprite = new Sprite(VisualAssets.food5);
                    break;
                default:
                    sprite = new Sprite(VisualAssets.food1);
                    break;
            }
        }
        sprite.setPosition(x - sprite.getWidth() / 2, y - sprite.getHeight() / 2);
        sprite.setScale(1.5f);
        rectangle = new Rectangle(x, y, sprite.getWidth() * 2.5f, sprite.getHeight() * 2.5f);

        gotEaten = false;
    }

    @Override
    public Vector2 getPosition() {
        return new Vector2(sprite.getX() + sprite.getWidth() / 2, sprite.getY() + sprite.getHeight() / 2);
    }

    @Override
    public Rectangle getRectangle() {
        return rectangle;
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        sprite.draw(spriteBatch);
    }

    public void eat() {
        gotEaten = true;
    }

    public boolean gotEaten() {
        return gotEaten;
    }

    public boolean isRainbofartItem() {
        return isRainbofartItem;
    }
}
