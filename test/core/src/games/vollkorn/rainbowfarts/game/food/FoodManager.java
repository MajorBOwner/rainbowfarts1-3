package games.vollkorn.rainbowfarts.game.food;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.UpdatableGameObject;

public class FoodManager implements DrawableGameObject, UpdatableGameObject {

    private ArrayList<Food> food;
    private ArrayList<Food> foodToBeRemoved;

    private float farticornHeight;

    private Vector2 lastSpawnPoint;

    private int starSpawnCounter;

    public FoodManager(float firstSpawnX, float firstSpawnY) {
        food = new ArrayList<Food>();
        foodToBeRemoved = new ArrayList<Food>();

        lastSpawnPoint = new Vector2(firstSpawnX, firstSpawnY);
        food.add(new Food(lastSpawnPoint.x, lastSpawnPoint.y - 500, false));
        food.add(new Food(lastSpawnPoint.x, lastSpawnPoint.y, false));
        food.add(new Food(lastSpawnPoint.x, lastSpawnPoint.y + 500, false));

        starSpawnCounter = 0;
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        for (Food f : food) {
            f.draw(spriteBatch);
        }
    }

    public ArrayList<Food> getFood() {
        return food;
    }

    @Override
    public void update(float deltaTime) {
        if (farticornHeight >= lastSpawnPoint.y - 3000) {
            int xShift = MathUtils.random(0, 1) * 400 - 200;
            lastSpawnPoint.x = lastSpawnPoint.x + xShift;
            if (lastSpawnPoint.x > 800) {
                lastSpawnPoint.x = 800;
            }
            if (lastSpawnPoint.x < -800) {
                lastSpawnPoint.x = -800;
            }
            lastSpawnPoint.y = lastSpawnPoint.y + 1000;
            food.add(new Food(lastSpawnPoint.x, lastSpawnPoint.y, false));

            starSpawnCounter += MathUtils.random(3);
            if (starSpawnCounter > 20) {
                starSpawnCounter = 0;
                if (lastSpawnPoint.x < 0) {
                    food.add(new Food(lastSpawnPoint.x + 800, lastSpawnPoint.y + MathUtils.random(-500, 500), true));
                } else {
                    food.add(new Food(lastSpawnPoint.x - 800, lastSpawnPoint.y + MathUtils.random(-500, 500), true));
                }
            }
        }

        for (Food f : food) {
            if (f.gotEaten() || f.getPosition().y < (farticornHeight - 10000)) {
                foodToBeRemoved.add(f);
            }
        }
        food.removeAll(foodToBeRemoved);
        foodToBeRemoved.clear();
    }

    public void setFarticornHeight(float farticornHeight) {
        this.farticornHeight = farticornHeight;
    }
}
