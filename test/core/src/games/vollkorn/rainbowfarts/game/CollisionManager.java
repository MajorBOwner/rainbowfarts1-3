package games.vollkorn.rainbowfarts.game;


import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

import games.vollkorn.framework.utils.Rectangle;
import games.vollkorn.framework.utils.UpdatableGameObject;
import games.vollkorn.rainbowfarts.game.Enemies.Enemy;
import games.vollkorn.rainbowfarts.game.Enemies.EnemyManager;
import games.vollkorn.rainbowfarts.game.food.Food;
import games.vollkorn.rainbowfarts.game.food.FoodManager;
import games.vollkorn.rainbowfarts.game.fartistuff.Farticle;
import games.vollkorn.rainbowfarts.game.fartistuff.Farticorn;

public class CollisionManager implements UpdatableGameObject {

    private Farticorn farticorn;

    private ArrayList<Farticle> farticles;

    private EnemyManager enemyManager;
    private FoodManager foodManager;

    private Rectangle collisionRectangle;
    private ArrayList<Enemy> collidingEnemies;

    private Rectangle leftWall, rightWall;
    private final float LEFTWALLX = -1000;
    private final float RIGHTWALLX = 1000;

    public CollisionManager(Farticorn farticorn, EnemyManager enemyManager, FoodManager foodManager, ParticleManager particleManager) {
        this.farticorn = farticorn;
        farticles = particleManager.getFarticles();
        this.enemyManager = enemyManager;
        this.foodManager = foodManager;

        collisionRectangle = new Rectangle(this.farticorn.getPosition().x, this.farticorn.getPosition().y, 3000, 3000);
        collidingEnemies = new ArrayList<Enemy>();

        leftWall = new Rectangle(LEFTWALLX, this.farticorn.getPosition().y, 100, 2000);
        rightWall = new Rectangle(RIGHTWALLX, this.farticorn.getPosition().y, 100, 2000);
    }

    @Override
    public void update(float deltaTime) {
        collideFarticornWithWalls();
        collisionRectangle.setCenter(farticorn.getPosition());
        collidingEnemies.clear();
        collideFarticornWithFood();
//        if(!farticorn.isRainbowfarting()){
        collideFarticornWithEnemies();
//        }
        collideFarticlesWithCollidingEnemies();
    }

    private void collideFarticornWithWalls() {
        leftWall.setCenter(LEFTWALLX, farticorn.getPosition().y);
        rightWall.setCenter(RIGHTWALLX, farticorn.getPosition().y);
        if (farticorn.getCircle().overlapRectangle(leftWall)) {
            Vector2 velocity = farticorn.getVelocity();
            if (velocity.x < 0) {
                velocity.x = 0;
            }
        }
        if (farticorn.getCircle().overlapRectangle(rightWall)) {
            Vector2 velocity = farticorn.getVelocity();
            if (velocity.x > 0) {
                velocity.x = 0;
            }
        }
    }

    private void collideFarticornWithEnemies() {
        for (Enemy enemy : enemyManager.getEnemies()) {
            if (farticorn.getCircle().overlapRectangle(enemy.getRectangle())) {
                farticorn.die();
            }

            if (collisionRectangle.overlapRectangle(enemy.getRectangle())) {
                collidingEnemies.add(enemy);
            }
        }
    }

    private void collideFarticornWithFood() {
        for (Food food : foodManager.getFood()) {
            if (farticorn.getCircle().overlapRectangle(food.getRectangle())) {
                food.eat();
                farticorn.eatFood();
                if (food.isRainbofartItem()) {
                    farticorn.activateRainbowFart();
                }
            }
        }
    }

    private void collideFarticlesWithCollidingEnemies() {
        for (Enemy enemy : collidingEnemies) {
            for (Farticle farticle : farticles) {
                if (farticle.getLifetime() > 0.1f) {
                    if (!farticle.didCollide()) {
                        if (farticle.getRectangle().overlapRectangle(enemy.getRectangle())) {
                            Vector2 direction = new Vector2(farticle.getVelocity().x, farticle.getVelocity().y);
                            direction.nor();
                            direction.x = -direction.x * (1 - farticle.getLifetime());
                            direction.y = -direction.y * (1 - farticle.getLifetime());
                            farticorn.fartpush(direction);
                            farticle.setDidCollide(true);
                            farticle.getVelocity().rotate(MathUtils.random(-90, 90));
                        }
                    }
                }
            }
        }
    }
}
