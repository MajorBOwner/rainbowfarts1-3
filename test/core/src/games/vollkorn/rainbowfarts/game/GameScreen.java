package games.vollkorn.rainbowfarts.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

import games.vollkorn.framework.assets.SoundAssets;
import games.vollkorn.framework.main.AbstractGameScreen;
import games.vollkorn.framework.main.ConfigConsts;
import games.vollkorn.framework.main.GameHeart;
import games.vollkorn.framework.main.ScreenRenderer;

public class GameScreen extends AbstractGameScreen {

    private GameController gameController;
    private ScreenRenderer screenRenderer;

    private boolean paused;
    private float timeAccumulator;

    public GameScreen(GameHeart game) {
        super(game);
        paused = false;
        timeAccumulator = 0.0f;
    }

    @Override
    public void render(float deltaTime) {
        timeAccumulator += deltaTime;
        while (timeAccumulator >= ConfigConsts.TIMESTEP) {
            // Do not update game world when paused.
            if (!paused) {
                // Update game world by the time that has passed since last rendered frame.
                gameController.update(ConfigConsts.TIMESTEP);
            }

            timeAccumulator -= ConfigConsts.TIMESTEP;
        }
        //Setting base render color
        Gdx.gl.glClearColor(ConfigConsts.BASE_RED, ConfigConsts.BASE_GREEN, ConfigConsts.BASE_BLUE, ConfigConsts.BASE_ALPHA);
        // Clears the screen
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        for (int i = 0; i < ConfigConsts.RENDER_STAGES; i++) {
            // Render game world to screen
            screenRenderer.render(i);
        }
    }

    @Override
    public void resize(int width, int height) {
        gameController.resizeAllViewports(width, height);
    }

    @Override
    public void show() {
        gameController = new GameController(game, this);
        screenRenderer = new ScreenRenderer(gameController);
//        gameController.setCamera(screenRenderer.getCamera());
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void hide() {
        screenRenderer.dispose();
        SoundAssets.gamemusic.stop();
        Gdx.input.setCatchBackKey(false);
    }

    @Override
    public void pause() {
        paused = true;
    }

    @Override
    public void resume() {
        super.resume();
// Only called on Android!
        paused = false;
    }
}
