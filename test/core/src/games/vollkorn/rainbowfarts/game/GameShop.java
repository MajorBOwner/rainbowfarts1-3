package games.vollkorn.rainbowfarts.game;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

import games.vollkorn.framework.assets.VisualAssets;
import games.vollkorn.framework.effects.AlphaSpriteEffect;
import games.vollkorn.framework.ui.Button;
import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.UpdatableGameObject;
import games.vollkorn.rainbowfarts.backelements.Star;

public class GameShop implements UpdatableGameObject, DrawableGameObject {

    private boolean isGameStarting;

    private Sprite farticorn, logo;
    private Animation<TextureRegion> logoAnimation;
    private float logoAnimationTimer;

    private Button playButton, noadButton, rankButton, rateButton, soundOnButton, soundOffButton;
    private int highscore;

    private boolean isSoundOn;

    private AlphaSpriteEffect logoAlphaEffect;

    private float animationTimer;
    private ParticleManager particleManager;

    public GameShop(boolean isSoundOn) {
        isGameStarting = false;

        logoAnimation = VisualAssets.logoAnimation;
        logo = new Sprite(VisualAssets.logo1);
        logo.setPosition(-logo.getWidth() / 2 - 20, 640);
        logo.setRotation(10);
        logoAnimationTimer = 0;

        farticorn = new Sprite(VisualAssets.shopicorn);
        farticorn.setPosition(-farticorn.getWidth() / 2, -150);
        particleManager = new ParticleManager();

        playButton = new Button(VisualAssets.play, VisualAssets.play, 0, 250);
        noadButton = new Button(VisualAssets.noad, VisualAssets.noad, 150, -250);
        rankButton = new Button(VisualAssets.rank, VisualAssets.rank, 400, -450);
        rateButton = new Button(VisualAssets.rate, VisualAssets.rate, -150, -350);
        soundOnButton = new Button(VisualAssets.soundon, VisualAssets.soundon, -400, -200);
        soundOffButton = new Button(VisualAssets.soundoff, VisualAssets.soundoff, -400, -200);

        this.isSoundOn = isSoundOn;

        logoAlphaEffect = new AlphaSpriteEffect(0.2f, 1, 0);

        animationTimer = 0;
    }

    public void turnOnOffSound(boolean soundOnOff) {
        isSoundOn = soundOnOff;
    }

    @Override
    public void update(float deltaTime) {
        animationTimer += deltaTime;
        playButton.update(deltaTime);
        noadButton.update(deltaTime);
        rankButton.update(deltaTime);
        rateButton.update(deltaTime);
        if (isSoundOn) {
            soundOnButton.update(deltaTime);
        } else {
            soundOffButton.update(deltaTime);
        }

        logoAnimationTimer += deltaTime;
        logoAlphaEffect.update(deltaTime);

        if (!isGameStarting) {
            particleManager.shootFarticle(-180, 180 + MathUtils.sin(animationTimer) * 30, -400, 300 * MathUtils.sin(animationTimer - 1f) + MathUtils.randomSign() * MathUtils.random(0, 200), 1f);
        }
        particleManager.update(deltaTime);
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        if (isGameStarting) {
            logoAlphaEffect.prepareSpriteBeforeDrawing(farticorn);
        } else {
            farticorn.setAlpha(1);
            farticorn.setPosition(-farticorn.getWidth() / 2, -150 + MathUtils.sin(animationTimer) * 30);
            farticorn.setRotation(MathUtils.cos(animationTimer) * 3);
        }
        particleManager.draw(spriteBatch);
        farticorn.draw(spriteBatch);

//        buttonParticleManager.draw(spriteBatch);
        //TODO Button Funktionen
        playButton.draw(spriteBatch);
        noadButton.setPosition(150 + MathUtils.sin(animationTimer - 2f) * 5, -250 + MathUtils.cos(animationTimer / 2.8f + 3f) * 10);
//        noadButton.draw(spriteBatch);
        rankButton.setPosition(400 + MathUtils.sin(animationTimer - 3f) * 5, -450 + MathUtils.cos(animationTimer / 1.8f + 2f) * 10);
//        rankButton.draw(spriteBatch);
        rateButton.setPosition(-150 + MathUtils.sin(animationTimer - 1f) * 5, -350 + MathUtils.cos(animationTimer / 1.5f + 1f) * 10);
//        rateButton.draw(spriteBatch);
        soundOnButton.setPosition(-400 + MathUtils.sin(animationTimer) * 5, -200 + MathUtils.cos(animationTimer / 2.1f) * 10);
        soundOffButton.setPosition(-400 + MathUtils.sin(animationTimer) * 5, -200 + MathUtils.cos(animationTimer / 2.1f) * 10);
        if (isSoundOn) {
            soundOnButton.draw(spriteBatch);
        } else {
            soundOffButton.draw(spriteBatch);
        }

        if (isGameStarting) {
            logoAlphaEffect.prepareSpriteBeforeDrawing(logo);
        } else {
            logo.setAlpha(1);
            logo.setScale(0.95f);
        }
        logo.setRegion(logoAnimation.getKeyFrame(logoAnimationTimer));
        logo.draw(spriteBatch);

        if (isGameStarting) {
            VisualAssets.pointsDrawer.setColor(1, 1, 1, logoAlphaEffect.getCurrentAlpha());
        } else {
            VisualAssets.pointsDrawer.setColor(1, 1, 1, 1);
        }
        VisualAssets.pointsDrawer.drawFromCenter(spriteBatch, highscore, 100, 0, -750);
    }

    public boolean isPlayPressed(float x, float y) {
        return playButton.isPressed(x, y);
    }

    public boolean isSoundPressed(float x, float y) {
        if (isSoundOn) {
            return soundOnButton.isPressed(x, y);
        } else {
            return soundOffButton.isPressed(x, y);
        }
    }

    public boolean isNoAdPressed(float x, float y) {
        return noadButton.isPressed(x, y);
    }

    public boolean isRankPressed(float x, float y) {
        return rankButton.isPressed(x, y);
    }

    public boolean isRatePressed(float x, float y) {
        return rateButton.isPressed(x, y);
    }

    public void startGame() {
        playButton.startAlphaEffect(0.2f, 1, 0);
        noadButton.startAlphaEffect(0.2f, 1, 0);
        rankButton.startAlphaEffect(0.2f, 1, 0);
        rateButton.startAlphaEffect(0.2f, 1, 0);
        soundOnButton.startAlphaEffect(0.2f, 1, 0);
        soundOffButton.startAlphaEffect(0.2f, 1, 0);
        logoAlphaEffect.start();
        isGameStarting = true;
    }

    public boolean canStartGame() {
        return true;
    }

    public void setHighscore(int highscore) {
        this.highscore = highscore;
    }

}
