package games.vollkorn.rainbowfarts.game;


import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import games.vollkorn.framework.utils.UpdatableGameObject;

public class SpawnPoint implements UpdatableGameObject {

    private Vector2 position;
    private float objectVelocityFactor;

    private float spawnTimer, maxSpawnTime;

    public SpawnPoint(float x, float y, float maxSpawnTime) {
        position = new Vector2(x, y);
        if (x < 0) {
            objectVelocityFactor = MathUtils.random(1f, 2f);
        } else {
            objectVelocityFactor = -MathUtils.random(1f, 2f);
        }
        spawnTimer = 0;
        this.maxSpawnTime = maxSpawnTime;
    }

    public float getObjectVelocityFactor() {
        return objectVelocityFactor;
    }

    public Vector2 getPosition() {
        return position;
    }

    @Override
    public void update(float deltaTime) {
        spawnTimer += deltaTime;
    }

    public float getSpawnTimer(){
        return spawnTimer;
    }

    public float getMaxSpawnTime(){
        return maxSpawnTime;
    }

    public void resetSpawnTimer(){
        spawnTimer = 0;
    }
}
