package games.vollkorn.rainbowfarts.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import games.vollkorn.framework.assets.SoundAssets;
import games.vollkorn.framework.main.AbstractScreenController;
import games.vollkorn.framework.main.ConfigConsts;
import games.vollkorn.framework.main.GameHeart;

public class GameController extends AbstractScreenController {

    enum GameState {
        SHOP,
        RUN,
        PAUSE
    }

    private OrthographicCamera uiCam, gameCam;
    private Viewport shopViewport, gameViewport;

    private WorldManager worldManager;
    private GameShop gameShop;

    private GameState gameState;

    private Vector2 screenTouchPosition;

    private int highscore;
    private boolean isSoundOn;

    private float runTime;

    private float cameraInterpolationFactor, shopcamInterpolationFactor, gamecamInterpolationFactor;

    private int adCounter;

    private long musicID;
    private float soundVolume, soundPitch, shopSoundVolume, runSoundVolume, shopSoundPitch, runSoundPitch;

    private int touchCounter;

    public GameController(GameHeart game, Screen screen) {
        super(game, screen);

        game.saveManager.load();
        highscore = game.saveManager.getInt("highscore");
        if (game.saveManager.hasEntry("isSoundOn")) {
            isSoundOn = game.saveManager.getBoolean("isSoundOn");
        } else {
            isSoundOn = true;
            game.saveManager.putBoolean("isSoundOn", true);
            game.saveManager.save();
        }
        SoundAssets.isSoundOn = isSoundOn;

        uiCam = new OrthographicCamera(ConfigConsts.VIEWPORTWIDTH, ConfigConsts.VIEWPORTHEIGHT);
        shopViewport = new FitViewport(uiCam.viewportWidth, uiCam.viewportHeight, uiCam);
        shopViewport.apply();
        shopViewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());


        gameCam = new OrthographicCamera(ConfigConsts.VIEWPORTWIDTH, ConfigConsts.VIEWPORTHEIGHT);
        gameViewport = new FitViewport(gameCam.viewportWidth, gameCam.viewportHeight, gameCam);
        gameViewport.apply();
        gameViewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        shopcamInterpolationFactor = 0.01f;
        gamecamInterpolationFactor = 0.09f;

        adCounter = -1;
        shopSoundPitch = 0.7f;
        shopSoundVolume = 0.1f;
        runSoundPitch = 1f;
        runSoundVolume = 0.2f;
        soundVolume = 0.1f;
        soundPitch = 0.7f;
        if (SoundAssets.isSoundOn) {
            musicID = SoundAssets.gamemusic.loop(shopSoundVolume, shopSoundPitch, 0);
            SoundAssets.gamemusic.setVolume(musicID, shopSoundVolume);
            SoundAssets.gamemusic.setPitch(musicID, shopSoundPitch);
        }
        initShop();

        touchCounter = 0;
    }

//    public void setCamera(OrthographicCamera camera) {
//        this.uiCam = camera;
//    }

    private void resetCameras() {
        uiCam.position.x = 0;
        uiCam.position.y = 0;
        uiCam.position.z = 0;
        uiCam.zoom = 1;
        uiCam.up.set(0, 1, 0);
        uiCam.direction.set(0, 0, -1);
        gameCam.position.x = 0;
        gameCam.position.y = 0;
        gameCam.position.z = 0;
        gameCam.zoom = 2;
        gameCam.up.set(0, 1, 0);
        gameCam.direction.set(0, 0, -1);
        cameraInterpolationFactor = shopcamInterpolationFactor;
    }

    private void initShop() {
        adCounter++;
        if (adCounter >= 5) {
            adCounter = 0;
            //TODO für android wieder reinnehmen
            game.androidCaller.showInterstitialAd();
        }
        resetCameras();
        gameShop = new GameShop(isSoundOn);
        gameState = GameState.SHOP;
        //pre init of worldManager for faster loading of actual game
        worldManager = new WorldManager();
        game.saveManager.putInt("highscore", highscore);
        game.saveManager.save();
        gameShop.setHighscore(highscore);
    }

    private void initRun() {
        screenTouchPosition = new Vector2();
        gameState = GameState.RUN;
        runTime = 0;
        gameShop.startGame();
    }

    @Override
    public void update(float deltaTime) {
        switch (gameState) {
            case SHOP:
                gameShop.update(deltaTime);
                worldManager.updateStartingStars(deltaTime);
                adjustVolumeAndPitch(deltaTime);
                break;
            case RUN:
                runTime += deltaTime;
                if (cameraInterpolationFactor < gamecamInterpolationFactor) {
                    cameraInterpolationFactor += deltaTime * 0.01f;
                    if (cameraInterpolationFactor >= gamecamInterpolationFactor) {
                        cameraInterpolationFactor = gamecamInterpolationFactor;
                    }
                }
                if (runTime <= 1f) {
                    gameShop.update(deltaTime);
                }
                Vector3 worldCoordinates = new Vector3(screenTouchPosition.x, screenTouchPosition.y, 0);
                gameCam.unproject(worldCoordinates);
                worldManager.setWorldTouchPosition(worldCoordinates.x, worldCoordinates.y);
                worldManager.update(deltaTime);
                gameCam.position.x = 0;
                gameCam.position.y += (worldManager.getMaxPlayerHeight() + 400 - gameCam.position.y) * cameraInterpolationFactor;
                if (gameCam.position.y > worldManager.getPlayerPosition().y + 2200) {
                    worldManager.killPlayer();
                }
                worldManager.setCameraPosition(gameCam.position.x, gameCam.position.y);
                if (worldManager.isPlayerDead()) {
                    if (worldManager.getHeight() > highscore) {
                        highscore = (int) worldManager.getHeight();
                    }
                    initShop();
                }
                adjustVolumeAndPitch(deltaTime);
                break;
            case PAUSE:
                adjustVolumeAndPitch(deltaTime);
                break;
        }

        if (SoundAssets.isSoundOn) {
            SoundAssets.gamemusic.setVolume(musicID, soundVolume);
            SoundAssets.gamemusic.setPitch(musicID, soundPitch);
        }
    }

    @Override
    public void render(SpriteBatch spriteBatch, int renderStage) {
        if (renderStage == 0) {  //gameCam
            switch (gameState) {
                case SHOP:
                    worldManager.drawBackground(spriteBatch);
                    break;
                case RUN:
                    worldManager.draw(spriteBatch);
                    break;
                case PAUSE:
                    break;
            }
        } else {  //uiCam
            switch (gameState) {
                case SHOP:
                    gameShop.draw(spriteBatch);
                    break;
                case RUN:
                    worldManager.drawUI(spriteBatch);
                    if (runTime <= 1f) {
                        gameShop.draw(spriteBatch);
                    }
                    break;
                case PAUSE:
                    break;
            }
        }
//        if (renderStage == 0) {
//            switch (gameState) {
//                case SHOP:
//                    gameShop.draw(spriteBatch);
//                    break;
//                case RUN:
//                    worldManager.draw(spriteBatch);
//                    break;
//                case PAUSE:
//                    break;
//            }
//            cameraHelper.snapshotCameraValues();
//            cameraHelper.resetCameraToZeroValues();
//            cameraHelper.applyTo(uiCam);
//        } else {
//            switch (gameState) {
//                case SHOP:
////                    gameShop.draw(spriteBatch);
//                    break;
//                case RUN:
//                    worldManager.drawUI(spriteBatch);
//                    gameShop.draw(spriteBatch);
//                    break;
//                case PAUSE:
//                    break;
//            }
//            cameraHelper.revertCameraValuesToSnapshot();
//            cameraHelper.applyTo(uiCam);
//        }
    }

    private void adjustVolumeAndPitch(float deltaTime) {
        switch (gameState) {
            case SHOP:
                if (soundVolume > shopSoundVolume) {
                    soundVolume -= deltaTime;
                    if (soundVolume <= shopSoundVolume) {
                        soundVolume = shopSoundVolume;
                    }
                }
                if (soundPitch > shopSoundPitch) {
                    soundPitch -= deltaTime;
                    if (soundPitch <= shopSoundPitch) {
                        soundPitch = shopSoundPitch;
                    }
                }
                break;
            case RUN:
                if (soundVolume < runSoundVolume) {
                    soundVolume += deltaTime;
                    if (soundVolume >= runSoundVolume) {
                        soundVolume = runSoundVolume;
                    }
                }
                if (soundPitch < runSoundPitch) {
                    soundPitch += deltaTime;
                    if (soundPitch >= runSoundPitch) {
                        soundPitch = runSoundPitch;
                    }
                }
                break;
            case PAUSE:
                if (soundVolume > shopSoundVolume) {
                    soundVolume -= deltaTime;
                    if (soundVolume <= shopSoundVolume) {
                        soundVolume = shopSoundVolume;
                    }
                }
                if (soundPitch > shopSoundPitch) {
                    soundPitch -= deltaTime;
                    if (soundPitch <= shopSoundPitch) {
                        soundPitch = shopSoundPitch;
                    }
                }
                break;
        }

    }

    @Override
    public OrthographicCamera getCameraAccordingToRenderStage(int renderStage) {
        if (renderStage == 0) {
            gameCam.update();
            return gameCam;
        } else {
            uiCam.update();
            return uiCam;
        }
    }

    @Override
    public void resizeAllViewports(int width, int height) {
        shopViewport.update(width, height);
        gameViewport.update(width, height);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        touchCounter++;
        switch (gameState) {
            case SHOP:
                Vector3 worldCoordinates = new Vector3(screenX, screenY, 0);
                uiCam.unproject(worldCoordinates);
                gameShop.isPlayPressed(worldCoordinates.x, worldCoordinates.y);
                gameShop.isNoAdPressed(worldCoordinates.x, worldCoordinates.y);
                gameShop.isRankPressed(worldCoordinates.x, worldCoordinates.y);
                gameShop.isRatePressed(worldCoordinates.x, worldCoordinates.y);
                gameShop.isSoundPressed(worldCoordinates.x, worldCoordinates.y);
                break;
            case RUN:
                screenTouchPosition.x = screenX;
                screenTouchPosition.y = screenY;
                worldManager.setTouching(true);
                break;
            case PAUSE:
                break;
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        touchCounter--;
        switch (gameState) {
            case SHOP:
                Vector3 worldCoordinates = new Vector3(screenX, screenY, 0);
                uiCam.unproject(worldCoordinates);
                if (gameShop.isPlayPressed(worldCoordinates.x, worldCoordinates.y)) {
                    initRun();
                }
                if (gameShop.isNoAdPressed(worldCoordinates.x, worldCoordinates.y)) {
                    //TODO
                }
                if (gameShop.isRankPressed(worldCoordinates.x, worldCoordinates.y)) {
                    //TODO
                }
                if (gameShop.isRatePressed(worldCoordinates.x, worldCoordinates.y)) {
                    //TODO
                }
                if (gameShop.isSoundPressed(worldCoordinates.x, worldCoordinates.y)) {
                    if (isSoundOn) {
                        isSoundOn = false;
                        game.saveManager.putBoolean("isSoundOn", false);
                    } else {
                        isSoundOn = true;
                        game.saveManager.putBoolean("isSoundOn", true);
                    }
                    gameShop.turnOnOffSound(isSoundOn);
                    game.saveManager.save();
                    SoundAssets.isSoundOn = isSoundOn;
                    if (SoundAssets.isSoundOn) {
                        musicID = SoundAssets.gamemusic.loop(shopSoundVolume, shopSoundPitch, 0);
                    } else {
                        SoundAssets.gamemusic.stop();
                    }
                }
                break;
            case RUN:
                screenTouchPosition.x = screenX;
                screenTouchPosition.y = screenY;
                if (touchCounter == 0) {
                    worldManager.setTouching(false);
                }
                break;
            case PAUSE:
                break;
        }
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        switch (gameState) {
            case SHOP:
                Vector3 worldCoordinates = new Vector3(screenX, screenY, 0);
                uiCam.unproject(worldCoordinates);
                gameShop.isPlayPressed(worldCoordinates.x, worldCoordinates.y);
                gameShop.isNoAdPressed(worldCoordinates.x, worldCoordinates.y);
                gameShop.isRankPressed(worldCoordinates.x, worldCoordinates.y);
                gameShop.isRatePressed(worldCoordinates.x, worldCoordinates.y);
                gameShop.isSoundPressed(worldCoordinates.x, worldCoordinates.y);
                break;
            case RUN:
                screenTouchPosition.x = screenX;
                screenTouchPosition.y = screenY;
                break;
            case PAUSE:
                break;
        }
        return true;
    }
}
