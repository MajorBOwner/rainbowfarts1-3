package games.vollkorn.rainbowfarts.game.Enemies;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import games.vollkorn.framework.utils.CollidingRectangleObject;
import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.Rectangle;
import games.vollkorn.framework.utils.UpdatableGameObject;

public abstract class Enemy implements UpdatableGameObject, DrawableGameObject, CollidingRectangleObject {

    @Override
    public abstract void update(float deltaTime);

    @Override
    public abstract Vector2 getPosition();

    @Override
    public abstract Rectangle getRectangle();

    @Override
    public abstract void draw(SpriteBatch spriteBatch);

    public abstract float getXVelocity();
}
