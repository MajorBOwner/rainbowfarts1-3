package games.vollkorn.rainbowfarts.game.Enemies;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;

import java.util.ArrayList;
import java.util.LinkedList;

import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.UpdatableGameObject;
import games.vollkorn.rainbowfarts.game.SpawnPoint;

public class EnemyManager implements UpdatableGameObject, DrawableGameObject {

    private LinkedList<Enemy> enemies;
    private ArrayList<Enemy> enemiesToBeRemoved;

    private LinkedList<SpawnPoint> spawnPoints;

    private float farticornHeight, spawnXDistance, spawnYDistance, despawnXDistance, previousSpawnHeight;
    private float pegasusSpeed;

    public EnemyManager(float firstSpawnY) {
        enemies = new LinkedList<Enemy>();
        enemiesToBeRemoved = new ArrayList<Enemy>();

        farticornHeight = 0;
        spawnXDistance = 1500;
        spawnYDistance = 2000;
        despawnXDistance = 2000;
        previousSpawnHeight = firstSpawnY;

        spawnPoints = new LinkedList<SpawnPoint>();
        for (int i = 0; i < 15; i++) {
            createSpawnPoint();
        }

        pegasusSpeed = 400;
    }

    public LinkedList<Enemy> getEnemies() {
        return enemies;
    }

    public void setFarticornHeight(float farticornHeight) {
        this.farticornHeight = farticornHeight;
    }

    @Override
    public void update(float deltaTime) {
        if (farticornHeight > spawnPoints.getLast().getPosition().y + 3000) {
            spawnPoints.removeLast();
        }

        if (farticornHeight > previousSpawnHeight - 10000) {
            createSpawnPoint();
        }

        for (SpawnPoint spawnPoint : spawnPoints) {
            spawnPoint.update(deltaTime);
            if (spawnPoint.getSpawnTimer() >= spawnPoint.getMaxSpawnTime()) {
                Pegasus pegasus = new Pegasus(spawnPoint.getPosition().x, spawnPoint.getPosition().y, spawnPoint.getObjectVelocityFactor() * pegasusSpeed, 0);
                enemies.add(pegasus);
                spawnPoint.resetSpawnTimer();
            }
        }

        for (Enemy enemy : enemies) {
            enemy.update(deltaTime);

            if ((enemy.getXVelocity() > 0 && enemy.getPosition().x > despawnXDistance) || (enemy.getXVelocity() < 0 && enemy.getPosition().x < -despawnXDistance)) {
                enemiesToBeRemoved.add(enemy);
            }
        }

        enemies.removeAll(enemiesToBeRemoved);
        enemiesToBeRemoved.clear();
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        for (Enemy enemy : enemies) {
            enemy.draw(spriteBatch);
        }
    }

    private void createSpawnPoint() {
        int r = MathUtils.random(0, 1);
        float newSpawnHeight = previousSpawnHeight + spawnYDistance + MathUtils.random(-200, 200);
        if (r == 0) {
            SpawnPoint spawnPoint = new SpawnPoint(-spawnXDistance, newSpawnHeight, 4);
            spawnPoints.addFirst(spawnPoint);
        } else {
            SpawnPoint spawnPoint = new SpawnPoint(spawnXDistance, newSpawnHeight, 4);
            spawnPoints.addFirst(spawnPoint);
        }
        previousSpawnHeight = newSpawnHeight;
    }
}
