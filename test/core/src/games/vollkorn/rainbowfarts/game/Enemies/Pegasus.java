package games.vollkorn.rainbowfarts.game.Enemies;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import games.vollkorn.framework.assets.VisualAssets;
import games.vollkorn.framework.utils.Rectangle;
import games.vollkorn.rainbowfarts.game.Enemies.Enemy;

public class Pegasus extends Enemy {

    private Sprite sprite;
    private Vector2 position, velocity;

    private Animation<TextureRegion> animation;

    private float animationTimer;

    private Rectangle rectangle;

    public Pegasus(float x, float y, float xVel, float yVel) {
        sprite = new Sprite(VisualAssets.peg1);
        animationTimer = 0;
        position = new Vector2(x, y);
        velocity = new Vector2(xVel, yVel);

        animation = VisualAssets.pegasusAni;

        rectangle = new Rectangle(position.x, position.y, sprite.getWidth() * 0.9f, sprite.getHeight() * 0.9f);
    }

    @Override
    public void update(float deltaTime) {
        animationTimer += deltaTime;
        rectangle.setCenter(position.x, position.y);
        position.x += velocity.x * deltaTime;
        position.y += velocity.y * deltaTime;
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        sprite.setRegion(animation.getKeyFrame(animationTimer));
        if (velocity.x < 0) {
            sprite.setBounds(position.x - animation.getKeyFrame(animationTimer).getRegionWidth() / 2, position.y, animation.getKeyFrame(animationTimer).getRegionWidth(), animation.getKeyFrame(animationTimer).getRegionHeight());
        } else {
            sprite.setBounds(position.x + animation.getKeyFrame(animationTimer).getRegionWidth() / 2, position.y, -animation.getKeyFrame(animationTimer).getRegionWidth(), animation.getKeyFrame(animationTimer).getRegionHeight());
        }
//        sprite.setBounds(position.x - animation.getKeyFrame(animationTimer).getRegionWidth() / 2, position.y, animation.getKeyFrame(animationTimer).getRegionWidth(), animation.getKeyFrame(animationTimer).getRegionHeight());
        sprite.setOriginCenter();
        sprite.setPosition(position.x - sprite.getWidth() / 2, position.y - sprite.getHeight() / 2);

        sprite.draw(spriteBatch);
    }

    @Override
    public Vector2 getPosition() {
        return position;
    }

    @Override
    public Rectangle getRectangle() {
        return rectangle;
    }

    public float getXVelocity() {
        return velocity.x;
    }
}
