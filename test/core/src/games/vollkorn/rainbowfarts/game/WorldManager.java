package games.vollkorn.rainbowfarts.game;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import games.vollkorn.framework.assets.VisualAssets;
import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.UpdatableGameObject;
import games.vollkorn.rainbowfarts.game.Enemies.EnemyManager;
import games.vollkorn.rainbowfarts.game.food.FoodManager;
import games.vollkorn.rainbowfarts.backelements.Background;
import games.vollkorn.rainbowfarts.game.fartistuff.Farticorn;

public class WorldManager implements UpdatableGameObject, DrawableGameObject {

    private enum RunState {
        START,
        RUN
    }

    private RunState runState;

    private Farticorn farticorn;
    private ParticleManager particleManager;
    private EnemyManager enemyManager;
    private FoodManager foodManager;
    private Background background;

    private Vector2 fartDirection;

    private boolean isPlayerTouching;

    private CollisionManager collisionManager;

    private float airTime, startingHeight, height;

    private GameUI gameUI;

    private float maxGameHeight, maxPlayerHeight;
//    private Sprite maxHeightBar;

    public WorldManager() {
        particleManager = new ParticleManager();
        farticorn = new Farticorn(particleManager, 0, 5000);
        enemyManager = new EnemyManager(10000);
        foodManager = new FoodManager(0, 9000);
        background = new Background();

        fartDirection = new Vector2();

        isPlayerTouching = false;

        collisionManager = new CollisionManager(farticorn, enemyManager, foodManager, particleManager);

        airTime = -farticorn.getTimeToEmptyFartEnergy();
//        System.out.println(airTime);
        startingHeight = farticorn.getPosition().y;
        height = 0.0f;
        maxGameHeight = 0.0f;
        maxPlayerHeight = 0.0f;
//        maxHeightBar = new Sprite(VisualAssets.fartbar2);
//        maxHeightBar.setScale(3, 0.5f);

        gameUI = new GameUI();

        runState = RunState.START;
    }

    @Override
    public void update(float deltaTime) {
        airTime += deltaTime;
        switch (runState) {
            case START:
                if (airTime < 0) {
                    farticorn.fart(new Vector2(0, 1));
                    farticorn.setFarting(true);
                    startingHeight = farticorn.getPosition().y;
                } else {
                    farticorn.setFarting(false);
                    runState = RunState.RUN;
//                    System.out.println(startingHeight);
                }
                break;
            case RUN:
                if (isPlayerTouching) {
                    farticorn.fart(fartDirection);
                    farticorn.setFarting(true);
                } else {
                    farticorn.setFarting(false);
                }
                if (!farticorn.isDead() && farticorn.getPosition().y < 7000) {
                    farticorn.die();
                }
                break;
        }
        background.update(deltaTime);
        particleManager.update(deltaTime);
        farticorn.update(deltaTime);
        foodManager.setFarticornHeight(farticorn.getPosition().y);
        foodManager.update(deltaTime);
        enemyManager.setFarticornHeight(farticorn.getPosition().y);
        enemyManager.update(deltaTime);
        if (!farticorn.isDead()) {
            collisionManager.update(deltaTime);
        }

        height = farticorn.getPosition().y - startingHeight;
        background.setRealHeight(farticorn.getPosition().y);
        height = height / 200;
        if (height > maxGameHeight) {
            maxGameHeight = height;
        }
        gameUI.setPlayerHeight(maxGameHeight);
        gameUI.setFartEnergyProportion(farticorn.getFartEnergyProportion());
        gameUI.update(deltaTime);
        if (farticorn.getPosition().y > maxPlayerHeight) {
            maxPlayerHeight = farticorn.getPosition().y;
        }

//        maxHeightBar.setPosition(-maxHeightBar.getWidth()/2, maxPlayerHeight);
//        float maxHeightBarAlpha = (maxPlayerHeight - farticorn.getPosition().y) / 500;
//        if(maxHeightBarAlpha >= 0.5f){
//            maxHeightBarAlpha = 0.5f;
//        }
//        if(maxHeightBarAlpha <= 0){
//            maxHeightBarAlpha = 0;
//        }
//        maxHeightBar.setAlpha(maxHeightBarAlpha);
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        background.draw(spriteBatch);
        particleManager.draw(spriteBatch);
        foodManager.draw(spriteBatch);
        farticorn.draw(spriteBatch);
        enemyManager.draw(spriteBatch);
        background.drawStartingClouds(spriteBatch);
//        maxHeightBar.draw(spriteBatch);
    }

    public void drawBackground(SpriteBatch spriteBatch) {
        background.draw(spriteBatch);
        background.drawStartingClouds(spriteBatch);
    }

    public void drawUI(SpriteBatch spriteBatch) {
        gameUI.draw(spriteBatch);
    }

    public Vector2 getPlayerPosition() {
        return farticorn.getPosition();
    }

    public float getMaxPlayerHeight() {
        return maxPlayerHeight;
    }

    public void setWorldTouchPosition(float x, float y) {
//        if(x < 0){  //falls spieler links auf dem Bildschirm drückt
//            fartDirection.x = -1;
//        }else{  //falls Spieler rechts auf dem Bildschirm drückt
//            fartDirection.x = 1;
//        }
//        fartDirection.y = 1;
        fartDirection.x = (x - farticorn.getPosition().x) / 1080;
        fartDirection.y = 1;
        fartDirection = fartDirection.nor();
    }

    public void setTouching(boolean isTouching) {
        isPlayerTouching = isTouching;
    }

    public boolean isPlayerDead() {
        return farticorn.isDead() && farticorn.isExplosionFinished();
    }

    public void killPlayer() {
        farticorn.die();
    }

    public void setCameraPosition(float x, float y) {
        background.setCameraPosition(x, y);
    }

    public float getHeight() {
        return maxGameHeight;
    }

    public void updateStartingStars(float deltaTime) {
        background.updateStartingStars(deltaTime);
    }
}
