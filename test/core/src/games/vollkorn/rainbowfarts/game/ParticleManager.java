package games.vollkorn.rainbowfarts.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.ArrayList;

import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.UpdatableGameObject;
import games.vollkorn.rainbowfarts.game.fartistuff.Farticle;

public class ParticleManager implements DrawableGameObject, UpdatableGameObject {

    private ArrayList<Farticle> farticles;
    private int farticleMaxCount, farticleIterator, colorIterator;

    private Color red, orange, yellow, green, blue, violet;

    public ParticleManager() {
        farticles = new ArrayList<Farticle>();
        farticleMaxCount = 100;
        farticleIterator = 0;
        for (int i = 0; i < farticleMaxCount; i++) {
            farticles.add(new Farticle());
        }

        red = new Color(230f / 255f, 52f / 255f, 42f / 255f, 1);
        orange = new Color(243f / 255f, 145f / 255f, 78f / 255f, 1);
        yellow = new Color(255f / 255f, 216f / 255f, 74f / 255f, 1);
        green = new Color(149f / 255f, 193f / 255f, 37f / 255f, 1);
        blue = new Color(119f / 255f, 202f / 255f, 226f / 255f, 1);
        violet = new Color(165f / 255f, 100f / 255f, 166f / 255f, 1);
        colorIterator = 0;
    }

    public void shootFarticle(float x, float y, float velX, float velY, float lifetime) {
        farticles.get(farticleIterator).setDidCollide(false);
        farticles.get(farticleIterator).setColor(Color.WHITE);
        farticles.get(farticleIterator).shoot(x, y, velX, velY, lifetime);
        farticleIterator++;
        if (farticleIterator >= farticleMaxCount) {
            farticleIterator = 0;
        }
    }

    public void shootRandomColoredFarticle(float x, float y, float velX, float velY, float lifetime) {
        farticles.get(farticleIterator).setDidCollide(false);
        farticles.get(farticleIterator).setColor(getColorForIterator());
        farticles.get(farticleIterator).shoot(x, y, velX, velY, lifetime);
        farticleIterator++;
        if (farticleIterator >= farticleMaxCount) {
            farticleIterator = 0;
        }
    }

    private Color getColorForIterator() {
        colorIterator++;
        if (colorIterator > 5) {
            colorIterator = 0;
        }
        switch (colorIterator) {
            case 0:
                return red;
            case 1:
                return orange;
            case 2:
                return yellow;
            case 3:
                return green;
            case 4:
                return blue;
            case 5:
                return violet;
            default:
                return red;
        }
    }

    public ArrayList<Farticle> getFarticles() {
        return farticles;
    }

    @Override
    public void update(float deltaTime) {
        for (Farticle farticle : farticles) {
            farticle.update(deltaTime);
        }
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        for (Farticle farticle : farticles) {
            if (farticle.isAlive()) {
                farticle.draw(spriteBatch);
            }
        }
    }
}
