package games.vollkorn.rainbowfarts.game;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import games.vollkorn.framework.assets.VisualAssets;
import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.UpdatableGameObject;

public class GameUI implements UpdatableGameObject, DrawableGameObject {

    private Sprite fartbar1, fartbar2, fartbar3;
    private int playerHeight;
    private float fartEnergyProportion;

    private float uiAlpha;

    public GameUI() {
        fartbar1 = new Sprite(VisualAssets.fartbar1);
        fartbar2 = new Sprite(VisualAssets.fartbar2);
        fartbar3 = new Sprite(VisualAssets.fartbar3);
        fartbar1.setPosition(-fartbar1.getWidth() / 2, -900);
        fartbar2.setPosition(-fartbar2.getWidth() / 2, -900);
        fartbar3.setPosition(-fartbar3.getWidth() / 2, -900);
        playerHeight = 0;
        fartEnergyProportion = 1;
        uiAlpha = 0;
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        if (uiAlpha < 0.5f) {
            fartbar1.setAlpha(uiAlpha);
        } else {
            fartbar1.setAlpha(0.5f);
        }
        fartbar2.setAlpha(uiAlpha);
        fartbar3.setAlpha(uiAlpha);
        fartbar1.draw(spriteBatch);
        spriteBatch.draw(fartbar2.getTexture(),
                fartbar2.getX() + fartbar2.getRegionWidth() / 2.f - (fartbar2.getRegionWidth() * fartEnergyProportion) / 2.f,
                fartbar2.getY(),
                fartbar2.getRegionX() + (int) (fartbar2.getRegionWidth() / 2.f - (fartbar2.getRegionWidth() * fartEnergyProportion) / 2.f),
                fartbar2.getRegionY(),
                (int) (fartbar2.getRegionWidth() * fartEnergyProportion),
                fartbar2.getRegionHeight());
        fartbar3.draw(spriteBatch);

        VisualAssets.pointsDrawer.setColor(1, 1, 1, 0.5f);
        VisualAssets.pointsDrawer.drawFromCenter(spriteBatch, playerHeight, 100, 0, 700);
    }

    public void setPlayerHeight(float playerHeight) {
        this.playerHeight = (int) playerHeight;
    }

    public void setFartEnergyProportion(float fartEnergyProportion) {
        this.fartEnergyProportion = fartEnergyProportion;
    }

    @Override
    public void update(float deltaTime) {
        if (uiAlpha < 1) {
            uiAlpha += deltaTime * 0.5f;
            if (uiAlpha >= 1) {
                uiAlpha = 1;
            }
        }
    }
}
