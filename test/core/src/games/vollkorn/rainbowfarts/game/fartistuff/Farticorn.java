package games.vollkorn.rainbowfarts.game.fartistuff;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import games.vollkorn.framework.assets.SoundAssets;
import games.vollkorn.framework.assets.VisualAssets;
import games.vollkorn.framework.utils.Circle;
import games.vollkorn.framework.utils.CollidingCircleObject;
import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.UpdatableGameObject;
import games.vollkorn.rainbowfarts.game.ParticleManager;

public class Farticorn implements UpdatableGameObject, DrawableGameObject, CollidingCircleObject {

    private enum State {
        FARTING,
        FALLING,
        DEAD
    }

    private Sprite sprite;
    private Vector2 position, velocity, fartingDirection;
    private float fartEnergy, maxFartEnergy, energyDecreasingFactor, energyIncreasingFactor, fartPushForce, fartForce, gravity;

    private Animation<TextureRegion> fartAnimation, fallAnimation;
    private Animation<TextureRegion> explosionAnimation;
    private Animation<TextureRegion> currentAnimation;

    private State state;
    private float animationTimer;
    private float deathTimer = 0;

    private Circle circle;

    private float farticleXPos, farticleYPos;

    private ParticleManager particleManager;

    private float eatingAnimationDuration, eatingAnimationTimer;

    private boolean playingFart, playingStutter;
    private long fartSoundID;

    private boolean isRainbowFarting;
    private float rainbowFartTimer, rainbowFartMaxTime;

    public Farticorn(ParticleManager particleManager, float x, float y) {
        this.particleManager = particleManager;
        sprite = new Sprite(VisualAssets.fart4);
        animationTimer = 0;
        position = new Vector2(x, y);
        velocity = new Vector2(0, 0);
        fartingDirection = new Vector2(0, 0);
        maxFartEnergy = 100;
        energyDecreasingFactor = 60;
        energyIncreasingFactor = 10;
        fartForce = 40;
        fartPushForce = 50;
        gravity = 20;
        fartEnergy = maxFartEnergy;

        fartAnimation = VisualAssets.fartAni;
        fallAnimation = VisualAssets.fallAni;
        explosionAnimation = VisualAssets.explosionAni;

        circle = new Circle(position.x, position.y, 10);

        eatingAnimationDuration = 0.6f;
        eatingAnimationTimer = eatingAnimationDuration;

        SoundAssets.fartstutter.stop();
        SoundAssets.fartstop.stop();
        SoundAssets.fart.stop();
        playingFart = false;
        playingStutter = false;

        isRainbowFarting = false;
        rainbowFartTimer = 0.0f;
        rainbowFartMaxTime = 3;
    }

    @Override
    public Vector2 getPosition() {
        return position;
    }

    @Override
    public Circle getCircle() {
        return circle;
    }

    @Override
    public void update(float deltaTime) {
        animationTimer += deltaTime;
        if (isRainbowFarting) {
            rainbowFartTimer += deltaTime;
        }
        if (rainbowFartTimer >= rainbowFartMaxTime) {
            isRainbowFarting = false;
        }
        switch (state) {
            case FARTING:
                currentAnimation = fartAnimation;
                if (!isRainbowFarting) {
                    fartEnergy -= energyDecreasingFactor * deltaTime;
                }
                if (fartEnergy <= 0) {
                    fartEnergy = 0;
                    if (!playingStutter) {
                        playingStutter = true;
                        playingFart = false;
                        if (SoundAssets.isSoundOn) {
                            fartSoundID = SoundAssets.fartstutter.loop();
                            SoundAssets.fartstop.play();
                            SoundAssets.fart.stop();
                        }
                    }
                    SoundAssets.fartstutter.setPitch(fartSoundID, MathUtils.random(0.7f, 1.3f));
                    SoundAssets.fartstutter.setVolume(fartSoundID, MathUtils.random(0.7f, 1f));
                } else {
                    if (!playingFart) {
                        playingFart = true;
                        playingStutter = false;
                        if (SoundAssets.isSoundOn) {
                            SoundAssets.fartstutter.stop();
                            SoundAssets.fartstop.stop();
                            fartSoundID = SoundAssets.fart.loop();
                        }
                    }
                    SoundAssets.fart.setPitch(fartSoundID, MathUtils.random(0.7f, 1.3f));
                    SoundAssets.fart.setVolume(fartSoundID, MathUtils.random(0.7f, 1f));
                    calculateFarticleShootingPosition();
                    if (isRainbowFarting) {
                        particleManager.shootRandomColoredFarticle(farticleXPos, farticleYPos, velocity.x - fartingDirection.x * 2000, velocity.y - fartingDirection.y * 2000, 0.4f);
                    } else {
                        particleManager.shootFarticle(farticleXPos, farticleYPos, velocity.x - fartingDirection.x * 2000, velocity.y - fartingDirection.y * 2000, 0.4f);
                    }
                }
                break;
            case FALLING:
                playingFart = false;
                playingStutter = false;
                SoundAssets.fartstutter.stop();
                SoundAssets.fartstop.stop();
                SoundAssets.fart.stop();
                currentAnimation = fallAnimation;
                fartEnergy += energyIncreasingFactor * deltaTime;
                if (fartEnergy >= maxFartEnergy) {
                    fartEnergy = maxFartEnergy;
                }
                break;
            case DEAD:
                deathTimer += deltaTime;
                if (deathTimer < 0.55f) {
                    animationTimer = 0;
                    sprite.setScale(Interpolation.circleOut.apply(1, 1.5f, deathTimer));
                } else {
                    currentAnimation = explosionAnimation;
                    sprite.setScale(1);
                }
                break;
        }

        if (state != State.DEAD) {
            eatingAnimationTimer += deltaTime;
            if (eatingAnimationTimer >= eatingAnimationDuration) {
                eatingAnimationTimer = eatingAnimationDuration;
            }
            float eatingProgress = Math.min(1f, eatingAnimationTimer / eatingAnimationDuration);
            sprite.setScale(Interpolation.elasticOut.apply(1.5f, 1, eatingProgress), Interpolation.elasticOut.apply(0.5f, 1, eatingProgress));

            velocity.y -= gravity;   //gravity
            if (velocity.y > 3000) {
                velocity.y = 3000;
            }

            position.x += velocity.x * deltaTime;
            position.y += velocity.y * deltaTime;

            circle.setCenter(position.x, position.y);
        }
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        updateSprite();
//        if (isRainbowFarting) {
//            sprite.setColor(Color.YELLOW.lerp(Color.ORANGE, rainbowFartTimer / rainbowFartMaxTime));
//        } else {
//            sprite.setColor(Color.WHITE);
//        }
        sprite.draw(spriteBatch);
    }

    private void updateSprite() {
        sprite.setRegion(currentAnimation.getKeyFrame(animationTimer));
        sprite.setBounds(position.x - currentAnimation.getKeyFrame(animationTimer).getRegionWidth() / 2, position.y, currentAnimation.getKeyFrame(animationTimer).getRegionWidth(), currentAnimation.getKeyFrame(animationTimer).getRegionHeight());
        sprite.setOriginCenter();
        sprite.setPosition(position.x - sprite.getWidth() / 2, position.y - sprite.getHeight() / 2);
        if (state == State.FARTING) {
            sprite.setRotation(fartingDirection.angle());
        }
        if (90 < sprite.getRotation() && sprite.getRotation() < 270) {
            sprite.setFlip(false, true);
        } else {
            sprite.setFlip(false, false);
        }
    }

    private void calculateFarticleShootingPosition() {
        updateSprite();
        float[] vertices = sprite.getVertices();
        if (sprite.isFlipY()) {
            farticleXPos = vertices[Batch.X2] * 0.4f + vertices[Batch.X1] * 0.2f + vertices[Batch.X3] * 0.2f + vertices[Batch.X4] * 0.2f;
            farticleYPos = vertices[Batch.Y2] * 0.4f + vertices[Batch.Y1] * 0.2f + vertices[Batch.Y3] * 0.2f + vertices[Batch.Y4] * 0.2f;
        } else {
            farticleXPos = vertices[Batch.X1] * 0.4f + vertices[Batch.X2] * 0.2f + vertices[Batch.X3] * 0.2f + vertices[Batch.X4] * 0.2f;
            farticleYPos = vertices[Batch.Y1] * 0.4f + vertices[Batch.Y2] * 0.2f + vertices[Batch.Y3] * 0.2f + vertices[Batch.Y4] * 0.2f;
        }
    }

    public void fart(Vector2 direction) {
        if (fartEnergy > 0) {
            velocity.x += direction.x * fartForce;
            velocity.y += direction.y * fartForce;
        }
        fartingDirection.x = direction.x;
        fartingDirection.y = direction.y;
    }

    public void setFarting(boolean isFarting) {
        if (state != State.DEAD) {
            if (isFarting) {
                state = State.FARTING;
            } else {
                state = State.FALLING;
            }
        }
    }

    public void fartpush(Vector2 direction) {
        velocity.x += direction.x * fartPushForce;
        velocity.y += direction.y * fartPushForce;
    }

    public void eatFood() {
        if (state != State.DEAD) {
            if (SoundAssets.isSoundOn) {
                long sid = SoundAssets.burp.play();
                SoundAssets.burp.setVolume(sid, MathUtils.random(0.5f, 0.7f));
                SoundAssets.burp.setPitch(sid, MathUtils.random(0.8f, 1.2f));
            }
            fartEnergy += 30;
            if (fartEnergy >= maxFartEnergy) {
                fartEnergy = maxFartEnergy;
            }
            eatingAnimationTimer = 0;
        }
    }

    public boolean isDead() {
        return (state == State.DEAD);
    }

    public boolean isExplosionFinished() {
        return (explosionAnimation.isAnimationFinished(animationTimer));
    }

    public void die() {
        if (state != State.DEAD) {
            state = State.DEAD;
            if (SoundAssets.isSoundOn) {
                long sid = SoundAssets.explosion.play();
                SoundAssets.explosion.setVolume(sid, 0.8f);
            }
            animationTimer = 0;
        }
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public float getFartEnergyProportion() {
        return fartEnergy / maxFartEnergy;
    }

    public float getTimeToEmptyFartEnergy() {
        return fartEnergy / energyDecreasingFactor;
    }

    public void activateRainbowFart() {
        isRainbowFarting = true;
        rainbowFartTimer = 0.0f;
    }

    public boolean isRainbowfarting() {
        return isRainbowFarting;
    }
}
