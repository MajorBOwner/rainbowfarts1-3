package games.vollkorn.rainbowfarts.game.fartistuff;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import games.vollkorn.framework.assets.VisualAssets;
import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.Rectangle;
import games.vollkorn.framework.utils.UpdatableGameObject;

public class Farticle implements UpdatableGameObject, DrawableGameObject {

    private Vector2 position, velocity;
    private float lifetime, maxLifeTime;

    private Rectangle rectangle;

    private Sprite sprite;

    private boolean isAlive, didCollide;

    public Farticle() {
        position = new Vector2(0, 0);
        velocity = new Vector2(0, 0);

        int r = MathUtils.random(1, 6);
        switch (r) {
            case 1:
                sprite = new Sprite(VisualAssets.fartcloud1);
                break;
            case 2:
                sprite = new Sprite(VisualAssets.fartcloud2);
                break;
            case 3:
                sprite = new Sprite(VisualAssets.fartcloud3);
                break;
            case 4:
                sprite = new Sprite(VisualAssets.fartcloud4);
                break;
            case 5:
                sprite = new Sprite(VisualAssets.fartcloud5);
                break;
            case 6:
                sprite = new Sprite(VisualAssets.fartcloud6);
                break;
        }
        sprite.setOriginCenter();
        sprite.setRotation(MathUtils.random(0, 360));

        rectangle = new Rectangle(0, 0, sprite.getWidth() / 2, sprite.getHeight() / 2);

        isAlive = false;
        didCollide = false;
    }

    public void shoot(float x, float y, float velX, float velY, float lifetime) {
        position.x = x;
        position.y = y;
        velocity.x = velX;
        velocity.y = velY;
        rectangle.setCenter(position);
        this.lifetime = lifetime;
        maxLifeTime = lifetime;
        isAlive = true;
    }

    public float getLifetime() {
        return lifetime;
    }

    @Override
    public void update(float deltaTime) {
        if (lifetime > 0) {
            lifetime -= deltaTime;
            position.x += velocity.x * deltaTime;
            position.y += velocity.y * deltaTime;
            rectangle.setCenter(position);
        } else {
            isAlive = false;
        }
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        //alpha mit lifetime abnehmen lassen
        sprite.setPosition(position.x - sprite.getWidth() / 2, position.y - sprite.getHeight() / 2);
        float alpha = 1.f / maxLifeTime * lifetime;
        if (alpha <= 0) {
            alpha = 0;
        }
        sprite.setAlpha(alpha);
        sprite.draw(spriteBatch);
    }

    public boolean isAlive() {
        return isAlive;
    }

    public boolean didCollide() {
        return didCollide;
    }

    public void setDidCollide(boolean didCollide) {
        this.didCollide = didCollide;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public void setColor(Color color) {
        sprite.setColor(color);
    }
}
