package games.vollkorn.rainbowfarts;

import games.vollkorn.framework.main.AndroidCaller;
import games.vollkorn.framework.main.GameHeart;
import games.vollkorn.framework.main.SaveManager;
import games.vollkorn.rainbowfarts.game.GameScreen;
import games.vollkorn.rainbowfarts.landingpage.LandingScreen;

public class GameMain extends GameHeart {

    public GameMain() {
        super();
    }

    public GameMain(AndroidCaller androidCaller) {
        super(androidCaller);
    }

    @Override
    public void create() {
        super.create();
        setScreen(new LandingScreen(this));
    }
}
