package games.vollkorn.rainbowfarts.backelements;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import games.vollkorn.framework.assets.VisualAssets;
import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.ParallaxObject;
import games.vollkorn.framework.utils.UpdatableGameObject;

public class Cloud implements UpdatableGameObject, DrawableGameObject, ParallaxObject {

    private Vector2 position;
    private float velocity;
    private Sprite sprite;
    private float parallaxFactor;

    public Cloud(float x, float y, float velocity, int cloudType, float parallaxFactor) {
        position = new Vector2(x, y);
        this.velocity = velocity;
        this.parallaxFactor = parallaxFactor;
        switch (cloudType) {
            case 1:
                sprite = new Sprite(VisualAssets.cloud1);
                break;
            case 2:
                sprite = new Sprite(VisualAssets.cloud2);
                break;
            case 3:
                sprite = new Sprite(VisualAssets.cloud3);
                break;
            case 4:
                sprite = new Sprite(VisualAssets.cloud4);
                break;
            case 5:
                sprite = new Sprite(VisualAssets.cloud5);
                break;
            case 6:
                sprite = new Sprite(VisualAssets.cloud6);
                break;
        }
    }

    public void setAlpha(float alpha) {
        sprite.setAlpha(alpha);
    }

    public float getY(){
        return sprite.getY();
    }

    @Override
    public void update(float deltaTime) {
        position.x += velocity * deltaTime;
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        sprite.draw(spriteBatch);
    }

    @Override
    public void updateParallaxPosition(float relativeX, float relativeY) {
        sprite.setPosition((position.x - relativeX) * parallaxFactor + relativeX - sprite.getWidth() / 2, (position.y - relativeY) * parallaxFactor + relativeY - sprite.getHeight() / 2);
    }
}
