package games.vollkorn.rainbowfarts.backelements;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import games.vollkorn.framework.assets.VisualAssets;
import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.ParallaxObject;
import games.vollkorn.framework.utils.UpdatableGameObject;

public class Star implements UpdatableGameObject, DrawableGameObject, ParallaxObject {

    private Vector2 position;
    private Sprite sprite;
    private float parallaxFactor;

    private float animationTimer;

    public Star(float x, float y, float scale, float parallaxFactor) {
        position = new Vector2(x, y);
        sprite = new Sprite(VisualAssets.star);
        sprite.setPosition(x, y);
        sprite.setScale(scale);
        animationTimer = MathUtils.random(0, MathUtils.PI);
        this.parallaxFactor = parallaxFactor;
    }

    public float getY(){
        return sprite.getY();
    }

    @Override
    public void update(float deltaTime) {
        animationTimer += deltaTime;
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        sprite.setAlpha(Math.abs(MathUtils.sin(animationTimer * 2)));
        sprite.draw(spriteBatch);
    }

    @Override
    public void updateParallaxPosition(float relativeX, float relativeY) {
        sprite.setPosition((position.x - relativeX) * parallaxFactor + relativeX - sprite.getWidth() / 2, (position.y - relativeY) * parallaxFactor + relativeY);
    }
}
