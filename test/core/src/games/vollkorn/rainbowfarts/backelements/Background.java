package games.vollkorn.rainbowfarts.backelements;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

import games.vollkorn.framework.assets.VisualAssets;
import games.vollkorn.framework.effects.AlphaSpriteEffect;
import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.UpdatableGameObject;

public class Background implements UpdatableGameObject, DrawableGameObject {
    private Sprite back, gameShopBack;
    private AlphaSpriteEffect gameShopBackAlphaEffect;
    private float backScale;
    private ArrayList<Cloud> clouds, cloudsToBeRemoved, startingClouds;
    private ArrayList<Star> stars, starsToBeRemoved, startingStars;
    private ArrayList<Mountain> mountains;
    private ArrayList<Tree> trees;
    private ArrayList<Shrub> shrubs;
    private ArrayList<Grass> grass;

    private float lastCloudSpawnY, lastStarSpawnY;

    private float height;

    private Vector2 cameraPosition;

    private float runTime;

    public Background() {
        backScale = 200;
        back = new Sprite(VisualAssets.background);
        back.setScale(backScale);
        back.setPosition(back.getX(), (back.getHeight() / 2) * backScale - 2000);
        gameShopBack = new Sprite(VisualAssets.background);
        gameShopBack.setPosition(-gameShopBack.getWidth() / 2, -gameShopBack.getHeight() / 2);
        gameShopBack.setScale(15);
        gameShopBackAlphaEffect = new AlphaSpriteEffect(0.4f, 1, 0);
        gameShopBackAlphaEffect.start();

        clouds = new ArrayList<Cloud>();
        stars = new ArrayList<Star>();
        mountains = new ArrayList<Mountain>();
        mountains.add(new Mountain(4000, -6500, 1, 4, 0.2f));
        mountains.add(new Mountain(-2000, -4000, 2, 4, 0.2f));
        trees = new ArrayList<Tree>();
        trees.add(new Tree(-3000, -6000, 1, 1.7f, 0.25f));
        trees.add(new Tree(0, -6000, 3, 1.7f, 0.25f));
        trees.add(new Tree(3000, -6000, 2, 1.7f, 0.25f));
        shrubs = new ArrayList<Shrub>();
        shrubs.add(new Shrub(-1300, -6000, 2, 1.7f, 0.3f));
        shrubs.add(new Shrub(1800, -6000, 1, 1.7f, 0.3f));
        grass = new ArrayList<Grass>();
        grass.add(new Grass(0, -5000, 1, 3, 0.35f));
        grass.add(new Grass(0, -5000, 2, 3, 0.36f));

        lastCloudSpawnY = 8000;
        lastStarSpawnY = 50000;
        cloudsToBeRemoved = new ArrayList<Cloud>();
        starsToBeRemoved = new ArrayList<Star>();

        startingClouds = new ArrayList<Cloud>();
        for (int i = 0; i < 120; i++) {
            Cloud cloud = new Cloud(MathUtils.random(-1000, 1000), MathUtils.random(2200, 7000), 0, MathUtils.random(1, 6), 1.1f);
            startingClouds.add(cloud);
        }

        startingStars = new ArrayList<Star>();
        float starParallaxFactor = 10;
        for (int x = 0; x <= 10; x++) {
            for (int y = 0; y <= 40; y++) {
                startingStars.add(new Star(-1300 * starParallaxFactor + x * 260 * starParallaxFactor + MathUtils.random(5, 255) * starParallaxFactor, -300 * starParallaxFactor + y * 50 * starParallaxFactor + MathUtils.random(5, 45) * starParallaxFactor, MathUtils.random(0.01f, 0.02f), 1f / starParallaxFactor));
            }
            for (int y = 0; y <= 10; y++) {
                startingStars.add(new Star(-1300 * starParallaxFactor + x * 260 * starParallaxFactor + MathUtils.random(5, 255) * starParallaxFactor, 400 * starParallaxFactor + y * 50 * starParallaxFactor + MathUtils.random(5, 45) * starParallaxFactor, MathUtils.random(0.02f, 0.05f), 1f / starParallaxFactor));
            }
            for (int y = 0; y <= 15; y++) {
                startingStars.add(new Star(-1300 * starParallaxFactor + x * 260 * starParallaxFactor + MathUtils.random(5, 255) * starParallaxFactor, 1000 * starParallaxFactor + y * 50 * starParallaxFactor + MathUtils.random(5, 45) * starParallaxFactor, MathUtils.random(0.05f, 0.07f), 1f / starParallaxFactor));
            }
        }

        height = 0;

        cameraPosition = new Vector2();

        runTime = 0;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public void setRealHeight(float height) {
        this.height = height;
    }

    public void setCameraPosition(float x, float y) {
        cameraPosition.x = x;
        cameraPosition.y = y;
    }

    @Override
    public void update(float deltaTime) {
        runTime += deltaTime;
        if (runTime >= 0.4f) {
            startingStars.clear();
        }
        gameShopBackAlphaEffect.update(deltaTime);
        if (cameraPosition.y > lastCloudSpawnY - 3000) {
            Cloud cloud = new Cloud(MathUtils.random(-1200, 1200), lastCloudSpawnY, MathUtils.random(-150, 150), MathUtils.random(1, 6), 0.9f);
            cloud.setAlpha(0.4f);
            clouds.add(cloud);
            lastCloudSpawnY += MathUtils.random(500, 1500);
        }
        for (Cloud cloud : clouds) {
            cloud.update(deltaTime);
        }
        if (cameraPosition.y > lastStarSpawnY - 30000) {
            Star star = new Star(MathUtils.random(-12000, 12000), lastStarSpawnY, MathUtils.random(0.05f, 0.15f), 0.1f);
            stars.add(star);
            lastStarSpawnY += MathUtils.random(500, 1500);
        }
        for (Star star : stars) {
            star.update(deltaTime);
        }
        if (cameraPosition.y > 10000) {
            startingClouds.clear();
            startingStars.clear();
        }
    }

    public void updateStartingStars(float deltaTime) {
        for (Star star : startingStars) {
            star.update(deltaTime);
        }
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        float[] backVertices = back.getVertices();
        if (height >= (backVertices[Batch.Y3] - 3000)) {
            back.setPosition(back.getX(), back.getY() + (height - (backVertices[Batch.Y3] - 3000)));
        }
        back.draw(spriteBatch);
        gameShopBackAlphaEffect.prepareSpriteBeforeDrawing(gameShopBack);
        gameShopBack.draw(spriteBatch);

        for (Star star : startingStars) {
            star.updateParallaxPosition(cameraPosition.x, cameraPosition.y);
            star.draw(spriteBatch);
        }
        for (Star star : stars) {
            star.updateParallaxPosition(cameraPosition.x, cameraPosition.y);
            star.draw(spriteBatch);
            if (star.getY() < cameraPosition.y - 5000) {
                starsToBeRemoved.add(star);
            }
        }
        stars.removeAll(starsToBeRemoved);
        starsToBeRemoved.clear();
        for (Mountain mountain : mountains) {
            mountain.updateParallaxPosition(cameraPosition.x, cameraPosition.y);
            mountain.draw(spriteBatch);
        }
        for (Tree tree : trees) {
            tree.updateParallaxPosition(cameraPosition.x, cameraPosition.y);
            tree.draw(spriteBatch);
        }
        for (Shrub shrub : shrubs) {
            shrub.updateParallaxPosition(cameraPosition.x, cameraPosition.y);
            shrub.draw(spriteBatch);
        }
        for (Grass g : grass) {
            g.updateParallaxPosition(cameraPosition.x, cameraPosition.y);
            g.draw(spriteBatch);
        }
        for (Cloud cloud : clouds) {
            cloud.updateParallaxPosition(cameraPosition.x, cameraPosition.y);
            cloud.draw(spriteBatch);
            if (cloud.getY() < cameraPosition.y - 15000) {
                cloudsToBeRemoved.add(cloud);
            }
        }
        clouds.removeAll(cloudsToBeRemoved);
        cloudsToBeRemoved.clear();
    }

    public void drawStartingClouds(SpriteBatch spriteBatch) {
        if (cameraPosition.y < 10000) {
            for (Cloud cloud : startingClouds) {
                cloud.updateParallaxPosition(cameraPosition.x, cameraPosition.y);
                cloud.draw(spriteBatch);
            }
        }
    }
}
