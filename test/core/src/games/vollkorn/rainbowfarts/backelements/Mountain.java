package games.vollkorn.rainbowfarts.backelements;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import games.vollkorn.framework.assets.VisualAssets;
import games.vollkorn.framework.utils.DrawableGameObject;
import games.vollkorn.framework.utils.ParallaxObject;

public class Mountain implements DrawableGameObject, ParallaxObject {

    private Vector2 position;
    private Sprite sprite;
    private float parallaxFactor;

    public Mountain(float x, float y, int mountainType, float scale, float parallaxFactor){
        position = new Vector2(x, y);
        this.parallaxFactor = parallaxFactor;
        switch (mountainType) {
            case 1:
                sprite = new Sprite(VisualAssets.mountain1);
                break;
            case 2:
                sprite = new Sprite(VisualAssets.mountain2);
                break;
        }
        sprite.setScale(scale);
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        sprite.draw(spriteBatch);
    }

    @Override
    public void updateParallaxPosition(float relativeX, float relativeY) {
        sprite.setPosition((position.x - relativeX) * parallaxFactor + relativeX - sprite.getWidth() / 2, (position.y - relativeY) * parallaxFactor + relativeY);
    }
}
